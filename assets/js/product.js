/* pro image delete */
$(document).on('click', '.delete_event_file', function () {
    if (confirm('This item will be permanently deleted and cannot be recovered. Are you sure?')) {
        $(this).closest('li').remove();
        $file_id = $(this).attr('rel');
        $.post(base_url + "admin/products/delete_pro_img/", {file_id: $file_id});
    }
});




jQuery('.delete').click(function (e) {
    e.stopPropagation();
    var product_id = $(this).attr('product_id');
    var answer = confirm("Are you sure you want to delete from the database?");
    if (answer) {
        $.ajax({
            type: "POST",
            url: base_url + 'admin/products/delete_product',
            data: "product_id=" + product_id,
            success: function (result) {
                var obj = jQuery.parseJSON(result);


                if (obj['status'] == true) {
                    window.location.href = base_url + 'admin/products';
                } else {
                    alert('Somthing wrong ');
                }

                //alert(result);return false;
            }
        });
    }


});

jQuery(document).on("change", ".product_category", function () {

    $cat_id = $(this).val();
    $.ajax({
        type: "POST",
        url: base_url + 'admin/products/get_related_subcat',
        data: "cat_id=" + $cat_id,
        success: function (result) {

            $('.sub-cat-div').html(result);
            $('.sub-cat-div').show();

            //alert(result);return false;
        }
    });
});

jQuery('.product-type').change(function (e) {

    $(".error").text('');  //reset all old error

    //$(this).closest('.form-group').find('.product-type').removeAttr('checked');
    //$(this).attr('checked','checked');
    $type = $(this).val();

    if ($type == 2) {
        $('.food-type').show();
        $('.plate_type').show();
        $('.weight-box').hide();
        $('.price-box').hide();       
    } else {
        $('.food-type').hide();
        $('.plate_type').hide();
        $('.weight-box').show();
        $('.price-box').show();
    }

    $.ajax({
        type: "POST",
        url: base_url + 'admin/products/get_cat_by_product_type',
        data: "type=" + $type,
        success: function (result) {

            $('.category-div').html(result);
            $('.category-div').show();

            $('.sub-cat-div').html('');
            $('.sub-cat-div').hide();

            //alert(result);return false;
        }
    });

});

$(document).on('click', '.addBtn', function () {
    $('.processing').show();
    var inputs = $(this).closest('form').find('[name]');

    var $fields = {};
    for (var i = 0; i < inputs.length; i++) {
        $fields[$(inputs[i]).attr('name')] = $(inputs[i]).val();
    }
    $fields['type'] = $('.product-type:checked').val();

    console.log($fields);
    // return false;

    $.ajax({
        type: "GET",
        url: base_url + 'admin/products/validations',
        data: $fields,
        success: function ($resp) {
            $('.processing').hide();

            $res = JSON.parse($resp);
            //console.log($res); return false;
            $(".error").text('');  //reset all old error

            if ($res.error == 1) {

                for (var key in $res) {
                    $("[name='" + key + "']").closest('.form-group').find('.vlError').text($res[key]);
                }
            } else {

                $('form').submit();
                //return true;

            }
        }
    });

});

$('.retailer-pro-delete').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('#ret_id').val(id);
    $("#retailerProDeleteModal").modal('show');
});