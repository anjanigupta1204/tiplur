<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MEDS | INDIA</title>
    <link href="<?php echo base_url("theme/site/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("theme/site/font-awesome/css/font-awesome.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("theme/site/css/animate.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("theme/site/css/style.css"); ?>" rel="stylesheet">
</head>

<body class="gray-bg logingScreenBack" >
	<?php $this->load->view($view); ?>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url("theme/site/js/jquery-2.1.1.js"); ?>"></script>
	</script><script src="<?php echo base_url("theme/site/js/jquery.validate.min.js"); ?>"></script>
    <script src="<?php echo base_url("theme/site/js/bootstrap.min.js"); ?>"></script>
	
</body>

</html>

<script>
$(document).ready(function() 
{
$("#reset_validation").validate({
        rules: {
            password: {
                required: true,
                minlength: 5
            },
            cpass: {
				required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "This Field Is Required",
                minlength: "Password must be of minimum 5 characters"
            },
            cpass: {
				required: "This Field Is Required",
                minlength: "Password must be of 5 characters",
                equalTo: "The confirm password does not match."
            }
        }
        
    });
 });	
</script>