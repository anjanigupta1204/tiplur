/*Manage Nofications */

//window.setInterval(event, 2000);
function event() {
    jQuery.ajax({
        url: base_url + "/admin/notification/get_notification",
        success: function (data) {
            //console.log(data);
            $resl = jQuery.parseJSON(data);

            $(".notification-outer").html($resl.html);

            if ($resl.badge_count > 0) {
                $("span.label-primary").text($resl.badge_count);
                $("span.label-primary").show();
            }

        }
    });
}

function update_notification() {

    $count = $(".label-primary").html();
    if ($count) {
        jQuery.ajax({
            url: base_url + "admin/notification/update_notification",
            success: function (data) {
                $(".label-primary").hide(); //red count
                //$(".label-primary").show();//whats new count count
            }
        });
    }
}

/* Nofication Close Buttons */
$('.notification a.close').click(function (e) {
    e.preventDefault();

    $(this).parent('.notification').fadeOut();
});

/*
 Check All Feature
 */
$(".check-all").click(function (e) {
    e.stopPropagation();
    if ($(this).is(':checked')) {
        $('input.child').prop('checked', true);
    }
    else {
        $('input.child').removeAttr('checked');
    }
});
// script for upload image
$('.user_pic').change(function (e) {
    $user_pic = e.target.files[0].name;
    $(".old_user_pic").val($user_pic);
});
/* Alert*/
jQuery(document).on('click', '.delete-btn', function () {
    console.log($('.child:checked').val());
    if (typeof $('.child:checked').val() != 'undefined') {
        var test = confirm("Are you sure you want to delete this Record?");
        if (test == true) {
            return true;
        } else {
            return false;
        }
    }
});
/* Alert*/
jQuery(document).on('click', '.delete-btn-single', function () {
    var test = confirm("Are you sure you want to delete this Record?");
    if (test == true) {
        return true;
    } else {
        return false;
    }

});