<!-- <nav class="navbar-default navbar-static-side sidePanelPading" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="active" >
                <a class="subLi" href="<?php echo base_url('/admin/dashboard') ?>"><i class="fa fa-home fa-lg"></i> <span class="nav-label">Home</span></a>
            </li>
            <li>
                <a class="subLi" href="<?php echo base_url('admin/states'); ?>"><i class="fa fa-users "></i> <span class="nav-label">State</span> </span></a>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-archive fa-lg"></i> <span class="nav-label">Products</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class=""><a href="<?php echo base_url('admin/products'); ?>">Manage Products</a></li>
                    <li><a href="<?php echo base_url('admin/products/create'); ?>">Add Products</a></li>
                    <li><a href="<?php echo base_url('admin/products/upload'); ?>">Products Upload</a></li>
                </ul>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-sitemap fa-lg"></i> <span class="nav-label">Category</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class=""><a href="<?php echo base_url('admin/category'); ?>">Manage category</a></li>
                    <li><a href="<?php echo base_url('admin/category/create'); ?>">Add category</a></li>
                </ul>
            </li>
            <li>
                <a class="subLi"><i class="fa fa-user fa-lg"></i> <span class="nav-label">Users </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('admin/users'); ?>">Retailers </a></li>
                    <li><a href="<?php echo base_url('admin/users/create'); ?>">Create User</a></li>
                </ul>
            </li>
            <li>
                <a class="subLi" href="<?php echo base_url('admin/users/customers'); ?>"><i class="fa fa-users "></i> <span class="nav-label">Customers</span> </span></a>
            </li>
            <li>
                <a  href="<?php echo base_url('admin/orders'); ?>" class="subLi"><i class="fa fa-cart-plus fa-lg"></i> <span class="nav-label">Orders</span> </span></a>
            </li>
            <li>
                <a class="subLi" href="<?php echo base_url('admin/notification'); ?>"><i class="fa fa-bell fa-lg"></i> <span class="nav-label">Notification</span></span></a>
            </li>
        </ul>
    </div>
</nav> -->
<!-- full width menu -->
<!-- <nav class="navbar-default full-width" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#side-menu" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Brand</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav" id="side-menu">
            <li class="active" >
                <a class="subLi" href="<?php echo base_url('/admin/dashboard') ?>"><i class="fa fa-home fa-lg"></i> <span class="nav-label">Home</span></a>
            </li>
            <li>
                <a class="subLi" href="<?php echo base_url('admin/states'); ?>"><i class="fa fa-users "></i> <span class="nav-label">State</span> </span></a>
            </li>
            <li class="dropdown">
                <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-archive fa-lg"></i> <span class="nav-label">Products</span> <span class="fa arrow"></span></a>
                <ul class="dropdown-menu">
                    <li class=""><a href="<?php echo base_url('admin/products'); ?>">Manage Products</a></li>
                    <li><a href="<?php echo base_url('admin/products/create'); ?>">Add Products</a></li>
                    <li><a href="<?php echo base_url('admin/products/upload'); ?>">Products Upload</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-sitemap fa-lg"></i> <span class="nav-label">Category</span> <span class="fa arrow"></span></a>
                <ul class="dropdown-menu">
                    <li class=""><a href="<?php echo base_url('admin/category'); ?>">Manage category</a></li>
                    <li><a href="<?php echo base_url('admin/category/create'); ?>">Add category</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="subLi dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-user fa-lg"></i> <span class="nav-label">Users </span> <span class="fa arrow"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url('admin/users'); ?>">Retailers </a></li>
                    <li><a href="<?php echo base_url('admin/users/create'); ?>">Create User</a></li>
                </ul>
            </li>
            <li>
                <a class="subLi" href="<?php echo base_url('admin/users/customers'); ?>"><i class="fa fa-users "></i> <span class="nav-label">Customers</span> </span></a>
            </li>
            <li>
                <a  href="<?php echo base_url('admin/orders'); ?>" class="subLi"><i class="fa fa-cart-plus fa-lg"></i> <span class="nav-label">Orders</span> </span></a>
            </li>
            <li>
                <a class="subLi" href="<?php echo base_url('admin/notification'); ?>"><i class="fa fa-bell fa-lg"></i> <span class="nav-label">Notification</span></span></a>
            </li>
        </ul>
    </div>
</nav> -->