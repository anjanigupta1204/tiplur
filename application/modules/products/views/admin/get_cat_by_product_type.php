<div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
    <div class="form-group formWidht">
        <label>Category <span style="color: red;">*</span></label>
        <select name="category_id" class="form-control m-b addContDrop <?php if($type == 1) { ?>product_category<?php } ?>" required>
            <option value="">Please select</option>
            <?php product_cat(false, $type); ?>
        </select>
        <span class='error vlError'><?php echo form_error('category_id'); ?></span>
    </div>
</div>