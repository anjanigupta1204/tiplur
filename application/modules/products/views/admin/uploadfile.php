<style>
    .ScrollStyle
    {
        max-height: 500px;
        overflow-y: scroll;
    }
</style>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/products' => 'Manage Products', 'admin/products/uploadfile' => 'View Products')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h4>List of Products:</h4>
                    </div>
                    <?php if (empty($error_data)): ?>
                        <div class="well1 form-horizontal1 ibox-content borderNone ">
                            <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                            <div class="scrollit  ScrollStyle" style="margin-bottom: 30px;">
                                <table class="table">
                                    <thead>
                                        <tr class="textAlign">
                                            <th class="table-header">Product name *</th>
                                            <th class="table-header">Description *</th>
                                            <th class="table-header">Category *</th>
                                            <th class="table-header">Sub category</th>
                                            <th class="table-header">Product type *</th>
                                            <th class="table-header">Price *</th>
                                            <th class="table-header">Special Price </th>
                                            <th class="table-header">Quantity *</th>
                                            <th class="table-header">Weight *</th>
                                            <th class="table-header">In store *</th>
                                            <th class="table-header">Is cod *</th>
                                            <th class="table-header">Retailer Id *</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($values)): $i = 0;
                                            foreach ($values as $key => $val):
                                                ?>
                                                <tr class = "table-rowd " >
                                                    <td >
                                                        <textarea  rows="2" cols="150"  required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['product_name']) && $val['product_name']) ? '' : 'field-required'; ?>" required name='product_name[<?php echo $i; ?>]' ><?php echo (isset($val['product_name']) && $val['product_name'] != '#') ? trim($val['product_name']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('product_name[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <textarea rows="2" cols="150" required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['description']) && $val['description'] != '#') ? '' : 'field-required'; ?>" name='description[<?php echo $i; ?>]' ><?php echo (isset($val['description']) && $val['description'] != '#') ? trim($val['description']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('description[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td valign="bottom">
                                                        <input type='text'  required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['category']) && $val['category'] != '#') ? '' : 'field-required'; ?>" name='category[<?php echo $i; ?>]' value='<?php echo (isset($val['category']) && $val['category'] != '#') ? $val['category'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('category[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td >
                                                        <input type='text' class="form-control inputWidhtOnewTwenty 
                                                               <?php echo (isset($val['sub_category']) && $val['sub_category'] != '#') ? '' : 'field-required'; ?>" name='sub_category[<?php echo $i; ?>]' value='<?php echo (isset($val['sub_category']) && $val['sub_category'] != '#') ? trim($val['sub_category']) : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('sub_category[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td >
                                                        <input type='text' required class="form-control inputWidhtNinty <?php echo (isset($val['product_type']) && $val['product_type'] != '#') ? '' : 'field-required'; ?>" name='product_type[<?php echo $i; ?>]' value='<?php echo (isset($val['product_type']) && $val['product_type'] != '#') ? $val['product_type'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('product_type[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td >
                                                        <input type='text' required class="form-control inputWidhtSixty <?php echo (isset($val['price']) && $val['price'] != '#') ? '' : 'field-required'; ?>" name='price[<?php echo $i; ?>]' value='<?php echo (isset($val['price']) && $val['price'] != '#') ? $val['price'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('price[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td >
                                                        <input type='text'  class="form-control inputWidhtNinty <?php echo (isset($val['special_price']) && $val['special_price'] != '#') ? '' : 'field-required'; ?>" name='special_price[<?php echo $i; ?>]' value='<?php echo (isset($val['special_price']) && $val['special_price'] != '#') ? $val['special_price'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('special_price[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td >
                                                        <input type='text' required class="form-control inputWidhtSixty <?php echo (isset($val['quantity']) && $val['quantity'] != '#') ? '' : 'field-required'; ?>" name='quantity[<?php echo $i; ?>]' value='<?php echo (isset($val['quantity']) && $val['quantity'] != '#') ? $val['quantity'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('quantity[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtSixty <?php echo (isset($val['weight']) && $val['weight'] != '#') ? '' : 'field-required'; ?>" name='weight[<?php echo $i; ?>]' value='<?php echo (isset($val['weight']) && $val['weight'] != '#') ? $val['weight'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('weight[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtSixty <?php echo (isset($val['in_store']) && $val['in_store'] != '#') ? '' : 'field-required'; ?>" name='in_store[<?php echo $i; ?>]' value='<?php echo (isset($val['in_store']) && $val['in_store'] != '#') ? $val['in_store'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('in_store[' . $i . ']'); ?></span>
                                                    </td>

                                                    <td>
                                                        <input type='text' required class="form-control inputWidhtSixty <?php echo (isset($val['is_cod']) && $val['is_cod'] != '#') ? '' : 'field-required'; ?>" name='is_cod[<?php echo $i; ?>]' value='<?php echo (isset($val['is_cod']) && $val['is_cod'] != '#') ? $val['is_cod'] : ''; ?>'>
                                                        <span class='text-danger'><?php echo form_error('is_cod[' . $i . ']'); ?></span>
                                                    </td>
                                                    <td >
                                                        <textarea  rows="2" cols="150"  required class="form-control inputWidhtOnewTwenty <?php echo (isset($val['retailer_id']) && $val['retailer_id']) ? '' : 'field-required'; ?>" required name='retailer_id[<?php echo $i; ?>]' ><?php echo (isset($val['retailer_id']) && $val['retailer_id'] != '#') ? trim($val['retailer_id']) : ''; ?></textarea>
                                                        <span class='text-danger'><?php echo form_error('retailer_id[' . $i . ']'); ?></span>
                                                    </td>

                                                </tr> 
                                                <?php
                                                $i++;
                                            endforeach;
                                        endif;
                                        ?>


                                        </td> 
                                    </tbody>
                                </table>
                            </div>
                            <?php if (!empty($values)): ?>
                                <input type='submit' name='upload' class='btn btn-primary createuser' value="Proceed Excel" /> &nbsp;OR&nbsp;     
                            <?php endif; ?>
                            <a  href="<?php echo site_url('/admin/products/upload'); ?>" class="btn btn-danger" style="padding: 5px 30px;">Cancel</a>
                            <?php echo form_close(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
