<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/products' => 'Product Managemen', 'admin/products/edit/' . @$this->uri->segment(4) => 'Update Products')); ?>

    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12" style="margin-top: 40px;">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">
                    <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                    <div class="ibox-title">
                        <h2 style="display: inline-block;">Update Products</h2>
                        <div class="ibox-tools" style="display: inline-block; float: right; top: -60px;">
                            <a class="btn btn-primary block full-width m-b catLogBtn" href="<?php echo base_url('admin/products/create'); ?>">ADD PRODUCT</a>
                        </div>
                    </div>

                    <div class="ibox-content contentBorder">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Product Type   <span>*</span></label><br>
                                    <label class="radio-inline">
                                        <input type="radio" class="product-type" value='1' <?php echo ($result->type == 1) ? "checked" : ""; ?> name="type">Liquor
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" class="product-type" value='2'  <?php echo ($result->type == 2) ? "checked" : ""; ?> name="type">Snacks
                                    </label>
                                    <span class='error vlError'><?php echo form_error('type'); ?></span>
                                </div>

                            </div>

                            <div class="category-div">
                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Category <span>*</span></label>
                                        <select name="category_id" class="form-control m-b addContDrop product_category">
                                            <option value="">Please select</option>
                                            <?php product_cat($result->category_id, $result->type); ?>
                                        </select>
                                        <span class='error vlError'><?php echo form_error('category_id'); ?></span>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="sub-cat-div" <?php if ($result->type != 1) { ?>style="display: none;"<?php } ?>>
                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Sub Category <span>*</span></label>
                                        <select name="sub_category_id"  class="form-control m-b addContDrop get_related_subcat">
                                            <?php product_cat_subcat($result->category_id, $result->sub_category_id); ?>
                                        </select>
                                        <span class='error vlError'><?php echo form_error('sub_category_id'); ?></span>
                                    </div>
                                </div>  
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Title <span>*</span></label>
                                    <input type="text" placeholder="Title" name="title" id="title" value="<?php echo set_value('title', trim($result->title)); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('title'); ?></span>
                                </div>
                            </div>                            

                            <div class="col-lg-9 col-md-9 col-sm-9 textAreaAddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Description <span>*</span></label>
                                    <textarea rows="4" type="description" name="description"  height200 placeholder="Description" id="description" class="form-control formWidht"><?php echo set_value('description', trim($result->description)); ?></textarea>
                                    <span class='error vlError'><?php echo form_error('description'); ?></span>
                                </div>

                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont price-box" <?php if ($result->type != 1) { ?>style="display: none;"<?php } ?>>
                                <div class="form-group formWidht">
                                    <label>Price <span>*</span></label>
                                    <input type="text" placeholder="product price" value="<?php if($result->type == 1){echo set_value('price', $result->price);} ?>" name="price" id="product-price" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('price'); ?></span>
                                </div>

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Special price </label>
                                    <input type="text" placeholder="Special price" value="<?php echo set_value('special_price', $result->special_price); ?>" name="special_price" id="Special" class="form-control formWidht">
                                </div>
                            </div>                            
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont weight-box" <?php if ($result->type != 1) { ?>style="display: none;"<?php } ?>>
                                <div class="form-group formWidht">
                                    <label>Quantity (ml)<span>*</span></label>
                                    <input type="text" placeholder="Quantity in ml" name="weight" id="weight" value="<?php if($result->type == 1){echo set_value('weight', explode('ml',$result->weight)[0]);} ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('weight'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 AddProdctInputCont plate_type" <?php if ($result->type != 2) { ?>style="display: none;"<?php } ?>>
                                <div class="form-group formWidht">
                                    <label>Full Plate Price<span style="color: red;">*</span></label>
                                    <input type="text" placeholder="Full Plate Price" name="full_price" value="<?php $full_price = isset(explode("/", $result->price)[1]) ? explode("/", $result->price)[1]:$result->price; echo set_value('full_price', $full_price); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('full_price'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-2 AddProdctInputCont plate_type" <?php if ($result->type != 2) { ?>style="display: none;"<?php } ?>>
                                <div class="form-group formWidht">
                                    <label>Half Plate Price</label>
                                    <input type="text" placeholder="Half Plate Price" name="half_price" value="<?php $half_plate = isset(explode("/", $result->price)[1]) ? explode("/", $result->price)[0]:''; echo set_value('half_price', $half_plate); ?>" class="form-control formWidht">
                                    <span class='error vlError'><?php echo form_error('half_price'); ?></span>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">

                                <div class="form-group formWidht text-center" style="padding-top: 14px">
                                    <br>
                                    <label>IS COD  <span>:- </span>
                                        <input type="checkbox" value='1' <?php echo (isset($result->is_cod) && $result->is_cod == 1) ? "checked" : ""; ?> name="is_cod">
                                    </label>

                                    <label style="margin-left:100px;">IN Store  <span>:- </span>
                                        <input type="checkbox" value='1' <?php echo (isset($result->in_store) && $result->in_store == 1) ? "checked" : ""; ?> name="in_store">
                                    </label>

                                </div>
                            </div> 
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Status   <span>*</span></label>
                                    <?php $check = ($result->status == 0) ? '2' : '1' ?>
                                    <select name="status" class="form-control m-b addContDrop ">
                                        <?php
                                        $status = status();
                                        foreach ($status as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == $check) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                        <span class='error vlError'><?php echo form_error('status'); ?></span>
                                    </select>
                                </div>
                            </div> 
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Retailer   <span style="color: red;">*</span></label>
                                    <select name="retailer_id" class="form-control m-b addContDrop" required>
                                        <option value="">Select</option>
                                        <?php
                                        $retailer = retailers();
                                        foreach ($retailer as $val):
                                            ?>
                                            <option value="<?php echo $val->id; ?>" <?php echo ($val->id == @$result->retailer_id) ? 'selected' : ''; ?>  ><?php echo $val->display_name; ?></option>
                                        <?php endforeach; ?>
                                        <span class='error vlError'><?php echo form_error('retailer_id'); ?></span>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont food-type" <?php if ($result->type != 2) { ?>style="display: none;"<?php } ?>>
                                <div class="form-group formWidht">
                                    <label>Food Type   <span style="color: red;">*</span></label>
                                    <select name="food_type" class="form-control m-b addContDrop" required>
                                        <?php
                                        $food_type = food_type();
                                        foreach ($food_type as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == $result->food_category) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                        <span class='error vlError'><?php echo form_error('food_type'); ?></span>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12  text-center" style="">
                                <input type="button" name="save" value="UPDATE PRODUCT" class="btn btn-primary block full-width m-b updateProductBtn addBtn"/>
                            </div>
                        </div>

                    </div> 

                </div> 
            </div>
        </div>
        </form>
    </div>
</div>
</div>			