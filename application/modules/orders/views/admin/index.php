<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/orders' => 'Order List')); ?>
    <div class="row border-bottom">

    </div>
    <!--<div class="row  border-bottom headWidth dashboard-header">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <h2 class="salesHead">Order List</h2>
                <select class="tableDrop headingSelect input-sm form-control input-s-sm inline">
                    <option value="0">Action</option>
                    <option value="1">Option 2</option>
                    <option value="2">Option 3</option>
                    <option value="3">Option 4</option>
                </select>
            </div>
        </div>
    </div>-->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php echo print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Retailer</label>
                                    <select class="form-control" name="retailer">
                                        <option value="">Please select</option>
                                        <?php
                                        $retailers = retailers();
                                        foreach ($retailers as $retailer) {
                                            ?>
                                            <option value="<?php echo $retailer->id; ?>" <?php if (isset($_GET['retailer']) && $_GET['retailer'] == $retailer->id) { ?>selected<?php } ?>><?php echo $retailer->display_name; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>                               
                            </div>

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                        <option value="">Please select</option>
                                        <?php
                                        $status = order_status();
                                        foreach ($status as $row) {
                                            ?>
                                            <option value="<?php echo $row->id; ?>" <?php if (isset($_GET['status']) && $_GET['status'] == $row->id) { ?>selected<?php } ?>><?php echo $row->title; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>From Date</label>
                                    <input type="text" class="form-control datepicker" name="from_dt" placeholder="From" value="<?php echo isset($_GET['from_dt']) ? $_GET['from_dt'] : '' ?>">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>To Date</label>
                                    <input type="text" class="form-control datepicker" name="to_dt" placeholder="To" value="<?php echo isset($_GET['to_dt']) ? $_GET['to_dt'] : '' ?>">
                                </div>
                            </div>

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/orders'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title borderNone">
                        <h2>Recent Orders </h2>                        
                        <span class="pull-right">
                            <a data-toggle="tooltip" title="Download Excel" href="<?php echo base_url('admin/orders/export_orders?').http_build_query($_GET);?>" class="">
                                <img src="<?php echo base_url('assets/images/exldwn.ico');?>" height="40" width="40">
                            </a>
                        </span>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Order No</th>
                                        <th>Customer</th>                                        
                                        <th>Retailer</th>
                                        <th>Shop</th>
                                        <th>Delivery Address</th>
                                        <th>Quantity</th>
                                        <th>Amount</th>
                                        <th>Payment Mode</th>
                                        <th>Order On</th>
                                        <th>Status</th>
                                        <th>View Details</th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <?php
                                    if (!empty($orders)):
                                        foreach ($orders as $order):
                                            ?>
                                            <tr <?php if ($order->status == '2') { ?>style="background-color:#ff9494;"<?php } ?>>
                                                <td>
                                                    <input type="checkbox"  class="i-checks child" name="id[]" value="<?php echo $order->id; ?>">
                                                </td>
                                                <td><?php echo $order->order_id; ?></td>
                                                <td><?php echo $order->user."(".$order->user_mobile.")"; ?></td>
                                                <td><?php echo $order->retailer."(".$order->retailer_mobile.")"; ?></td>
                                                <td><?php echo store_detail_by_retailer_id($order->retailer_id)->store_name;?></td>
                                                <td><?php echo $order->address . "," . $order->city . "," . $order->state . "," . $order->country; ?></td>
                                                <td><?php echo $order->quantity; ?></td>
                                                <td><?php echo $order->amount; ?></td>
                                                <td><?php echo $order->title; ?></td>                                                
                                                <td><?php echo site_date_time($order->order_date); ?></td>
                                                <td><?php echo print_order_status($order->status); ?></td>
                                                <td><a href="<?php echo base_url('admin/orders/view_order?id=') . $order->order_id; ?>"><i class="fa fa-eye"></i></a></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        echo "<tr><td colspan='12'>No Result Found</td></tr>";
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
</div>




