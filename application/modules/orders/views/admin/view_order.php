<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php //breadcrumbs(array('admin/orders' => 'Order List')); ?>
    <div class="row border-bottom">

    </div>
    <!--<div class="row  border-bottom headWidth dashboard-header">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <h2 class="salesHead">Order List</h2>
                <select class="tableDrop headingSelect input-sm form-control input-s-sm inline">
                    <option value="0">Action</option>
                    <option value="1">Option 2</option>
                    <option value="2">Option 3</option>
                    <option value="3">Option 4</option>
                </select>
            </div>
        </div>
    </div>-->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">                
                <div class="ibox float-e-margins">
                    <div class="ibox-title borderNone">
                        <h2>Order Details </h2>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>                                        
                                        <th>Order No</th>                                        
                                        <th><?php echo (isset($_GET['type']) && $_GET['type'] == 'user') ? 'Retailer' : 'Order By'; ?></th>
                                        <th>Order Items</th>
                                        <th>Order Total</th>
                                        <th>Order On</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <?php
                                    if (!empty($ids)):
                                        foreach ($ids as $id):
                                            $order = get_order_details($id);
                                            ?>
                                            <tr>
                                                <td><?php echo $order->order_id; ?></td>
                                                <td><?php echo (isset($_GET['type']) && $_GET['type'] == 'user') ? $order->retailer : $order->user; ?></td>
                                                <td><ul><?php
                                                        $products = explode('<@>', $order->product_id);
                                                        $quantity = explode('<@>', $order->quantity);
                                                        $price = explode('<@>', $order->price);
                                                        $i = 0;
                                                        foreach ($products as $product) {
                                                            ?>

                                                            <li><?php echo $product . "($order->weight) <strong>x</strong> " . $quantity[$i] . ' = Rs.' . ($price[$i] * $quantity[$i]); ?></li>

                                                            <?php
                                                            $i++;
                                                        }
                                                        ?></ul></td>
                                                <td><?php echo 'Rs.' . $order->total; ?></td>                                                                                                                                 
                                                <td><?php echo site_date_time($order->creation_date); ?></td>                                              
                                                <td><?php echo print_order_status($order->status); ?></td>
                                                <td><?php if ($order->status == 7) { ?><button type="button" class="btn btn-default order-decline" data-id="<?php echo $order->order_id; ?>">Decline Order</button><?php } ?></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        echo "<tr><td colspan='12'>No Result Found</td></tr>";
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="declineOrderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form action="<?php echo base_url('admin/orders/decline_order'); ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Decline Order</h4>
                </div>
                <div class="modal-body">                    
                    <div class="form-group formWidht">
                        <label>Decline Reason <span style="color: red;">*</span></label>
                        <input type="text" placeholder="" required name="reason" value="<?php echo set_value('reason', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                    </div>
                    <input type="hidden" id="order_id" name="order_id" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>				
                </div>
            </div>
        </form>
    </div>
</div>



