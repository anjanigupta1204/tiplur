<?php

class Orders extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('orders_model');
        $this->load->library('auth');
        $this->load->library('form_validation');

        $this->set_data = array('theme' => 'admin'); //define theme name 
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
    }

    function index() {
        is_admin();
        //pagination 
        $this->load->library('pagination');
        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/orders') : base_url(SITE_AREA . '/orders?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->orders_model->orders_listing($this->filter());
        $config = pagination_formatting();
        $limit = 20;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $data['orders'] = $this->orders_model->orders_listing($this->filter(), $limit, $offset, false);
        //end here 
        //prd($data['orders']);

        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js', '/assets/js/orders.js');
        $this->theme($data);
    }

    function filter() {
        return $this->input->get(array('retailer', 'status', 'from_dt', 'to_dt'));
    }

    function view_order() {

        $filter = $this->input->get(array('id', 'user_id', 'type'));

        $ids = array();

        if (isset($filter['id'])) {
            array_push($ids, $filter['id']);
        } else {
            if (isset($filter['user_id']) && isset($filter['type'])) {

                if ($filter['type'] == 'retailer' || $filter['type'] == 'user') {
                    $ids = $this->orders_model->get_user_orders($filter['user_id'], $filter['type']);
                }
            }
        }

        $data['ids'] = $ids;
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js', '/assets/js/orders.js');

        $this->theme($data);

        //prd($order_details);
    }

    public function export_orders() {

        $filter = isset($_GET['filter']) ? $this->filter() : false;

        $reports = $this->orders_model->orders_listing($filter, false, false, false);
        //echo "<pre>"; print_r($reports);die;

        /* Start Logic for excel */
        $exceldata = array();
        $table = '<table><tr><th>S.No</th><th>Order No</th><th>Customer</th><th>Retailer</th><th>Shop</th><th>Delivery Address</th><th>Quantity</th><th>Amount</th><th>Payment Mode</th><th>Order On</th><th>Status</th></tr>';
        $i = 1;
        foreach ($reports as $record) {
            //print_r($reports); 

            $table .= '<tr>';
            $table .='<td>' . $i . '</td>';
            $table .= '<td>' . $record->order_id . '</td>';
            $table .= '<td>' . $record->user . '</td>';
            $table .= '<td>' . $record->retailer . '</td>';
            $table .= '<td>' . store_detail_by_retailer_id($record->retailer_id)->store_name . '</td>';
            $table .='<td>' . $record->address . ',' . $record->city . ',' . $record->state . ',' . $record->country . '</td>';
            $table .= '<td>' . $record->quantity . '</td>';
            $table .= '<td>' . $record->amount . '</td>';
            $table .= '<td>' . $record->title . '</td>';
            $table .= '<td>' . date('d M Y', strtotime($record->creation_date)) . '</td>';
            $table .='<td>' . print_order_status($record->status) . '</td>';
            $table .= '</tr>';
            $i++;
        }
        $table .= '</table>';
        $date = date('d-m-Y');

        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=Tipular Order Details(" . $date . ").xls");
        echo $table;
        die();
        /* end Logic for excel */
    }
    
    function decline_order() {

        if (empty($_POST)) {
            show_404();
        }

        $id = $this->input->post('order_id');
        $reason = $this->input->post('reason');

        $order_details = get_order_details($id);
        //prd($order_details);
        $data = array('status' => '2', 'modification_date' => date('Y-m-d H:i:s'));

        $result = true;
        $updated = $this->orders_model->update_order($data, $id, $reason);
        //echo $updated;exit;
        if ($updated == false) {
            $result = false;
        }

        if ($result) {

            $order_details = get_order_details($id);
            
            $notify_data = array(
                'order_id' => 'Order Declined',
                'notified_to' => $order_details->user_id,
                'requested_by' => $this->session->userdata('id'),
                'type' => ORDER_DECLINED_NOTIFICATION,
                'title' => 'Order Declined',
                'message' => 'Your order is declined by admin',
                'reason' => $reason,
            );

            $notify = new Push_notification();
            $notify->send_notification($notify_data);

            //$email = $order_details->user_email;

            //$this->send_welcome_mail($email); //send mail

            set_flash_message($type = 'success', $message = 'Order declined successfully.');
            redirect('admin/orders');
        } else {
            set_flash_message($type = 'error', $message = 'something wrong.');
            redirect('admin/orders');
        }
    }

}
