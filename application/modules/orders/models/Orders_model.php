<?php

class Orders_model extends CI_Model {

    protected $prescriptionTable = "uploaded_prescription";
    protected $orderTable = "order";
    protected $cartTable = "cart";

    function __construct() {
        parent::__construct();
    }

    //order listing

    public function orders_listing($filter = null, $limit = null, $offset = null, $count = true) {

        if (isset($filter['status']) && $filter['status'] != '') {
            $this->db->where("order_detail.status", trim($filter['status']));
        }

        if (isset($filter['retailer']) and $filter['retailer']) {
            $this->db->where("retailer_id", $filter['retailer']);
        }

        //date filter
        if (isset($filter['from_dt']) && isset($filter['to_dt']) && $filter['from_dt'] != '' && $filter['to_dt'] != '') {
            $this->db->where('DATE(ti_order_detail.creation_date) >=', $filter['from_dt']);
            $this->db->where('DATE(ti_order_detail.creation_date) <=', $filter['to_dt']);
        } else if (isset($filter['from_dt']) && $filter['from_dt'] != '') {
            $this->db->where('DATE(ti_order_detail.creation_date) >=', $filter['from_dt']);
        } else if (isset($filter['to_dt']) && $filter['to_dt'] != '') {
            $this->db->or_where('DATE(ti_order_detail.creation_date) <=', $filter['to_dt']);
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        $this->db->select('order_detail.*,order_detail.creation_date as order_date,u1.display_name as retailer,u2.display_name as user,u1.mobile as retailer_mobile,u2.mobile as user_mobile,payment_option.title,delivery_address.*,SUM(price) as amount,order_detail.status');
        $this->db->join('users u1', 'u1.id = order_detail.retailer_id', 'left');
        $this->db->join('users u2', 'u2.id = order_detail.user_id', 'left');
        $this->db->join('payment_option', 'payment_option.id = order_detail.payment_mode', 'left');
        $this->db->join('delivery_address', 'delivery_address.id = order_detail.address', 'left');
        $this->db->order_by('order_detail.creation_date', 'desc');
        $this->db->group_by('order_detail.order_id');
        $res = $this->db->get('order_detail');

        if ($count) {
            //echo $this->db->last_query(); die;
            return $res->num_rows();
        } else {
            //echo $this->db->last_query(); die;
            return $res->result();
        }
    }

    public function get_user_orders($id, $type) {

        if ($type == 'retailer') {
            $this->db->where('retailer_id', $id);
        } else {
            $this->db->where('user_id', $id);
        }

        $res = $this->db->group_by('order_id')->get('order_detail')->result_array();

        //echo $this->db->last_query(); die;
        return array_column($res, 'order_id');
    }

    public function update_order($data, $order_id, $reason) {

        $this->db->where('order_id', $order_id);
        $result = $this->db->update('order_detail', $data);
        if($result){
        	$d=array();
        	$d['order_number'] = $order_id;
        	$d['comments'] = $reason;
        	$d['status'] = $data['status'];
        	$res = $this->db->insert('trans_order_status', $d);
        	return $res;
        }else{
        	return false;
        }
        
    }

}
