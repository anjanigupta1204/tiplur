<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/states' => 'Manage State')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 " style="margin-top: 40px;">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>State Name</label>
                                    <input type="text" class="form-control" name="title" placeholder="Name" value="<?php echo isset($_GET['title']) ? $_GET['title'] : '' ?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Age Limit</label>
                                    <select class="form-control" name="age">
                                        <option value="">Please select</option>
                                        <?php
                                        for ($i = 15; $i <= 100; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>" <?php if (isset($_GET['age']) && $_GET['age'] == $i) { ?>selected<?php } ?>><?php echo $i; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>                               
                            </div>

                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>Status</label>
                                    <?php
                                    $state_id = isset($_GET['status']) ? $_GET['status'] : '';
                                    status_dropdown($state_id, 'form-control');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/states'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2>View State </h2>
                        <div class="ibox-tools" style="display: inline-block; float: right;">  
                            <a href="<?php echo base_url('admin/states/create'); ?>" class="btn btn-primary block full-width m-b createuser">ADD STATE</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'product-listing', 'method' => 'get')); ?>
                        <div class="ibox-content borderNone">

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></td>
                                        <th>State Name</th>                                        
                                        <th>Age</th>
                                        <th>Opening Time</th>
                                        <th>Closing Time</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    <?php
                                    if ($result):
                                        foreach ($result as $val):
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="product_id[]" value="<?php echo $val->id; ?>">
                                                </td>
                                                <td><?php echo ucfirst($val->name); ?></td>                                                
                                                <td><?php echo ($val->age) ? $val->age : 'NA'; ?></td>
                                                <td><?php echo ($val->opening_time) ? $val->opening_time : 'NA'; ?></td>
                                                <td><?php echo ($val->closing_time) ? $val->closing_time : 'NA'; ?></td>
                                                <td><?php echo print_status($val->status); ?>	</td>
                                                <td class="text-center">
                                                    <a  href="<?php echo base_url('admin/states/edit' . '/' . $val->id); ?>" ><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                                    <a style="padding-left: 30px;" href="<?php echo base_url('admin/states/view' . '/' . $val->id); ?>" title="View Holidays"><i class="fa fa-file" aria-hidden="true"></i></a>
                                                </td>
                                            </tr> 
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <tr><td colspan="12">Result not found.</td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>

        </div>
    </div>
</div>



<!---modal open for image-->
<div id="imagemodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel">Images</h5>
                <button type="button" class="close closeBtnPopup" data-dismiss="modal" aria-label="Close">x
                </button>
            </div>
            <div class="modal-body">
                <div id="show_img_slider"></div>
            </div>
        </div>
    </div>
</div>

<!---close--->
</div>