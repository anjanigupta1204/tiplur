<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/states/view_hoildays' => 'Holiday List')); ?>
    <div class="row border-bottom">

    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php echo print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>State</label>
                                    <?php
                                    $state_id = isset($_GET['state_id']) ? $_GET['state_id'] : '';
                                    state_dropdown($state_id, 'form-control');
                                    ?>                                     
                                </div>
                            </div>

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/states/view_holidays'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title borderNone">
                        <h2>Holiday List </h2>
                        <div class="ibox-tools" style="display: inline-block; float: right;">  
                            <a href="<?php echo base_url('admin/states/add_holiday'); ?>" class="btn btn-primary block full-width m-b createuser">ADD HOLIDAY</a>
                        </div>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
						<?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => '', 'method' => 'get')); ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
										<td class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></td>									
                                        <th>State</th>
                                        <th>Holiday</th>
                                        <th>Holiday Date</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <?php
                                    if (!empty($records)):
                                        foreach ($records as $record):
                                            ?>
                                            <tr>
												<td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="holiday_id[]" value="<?php echo $record->id; ?>">
                                                </td>											
                                                <td><?php echo $record->state; ?></td>
                                                <td><?php echo $record->title; ?></td>
                                                <td><?php echo $record->holiday_date; ?></td>                                                
                                                <td><?php echo $record->description; ?></td>                                                
                                                <td><a href="<?php echo base_url('admin/states/edit/') . $record->state_id; ?>"><i class="fa fa-pencil-square-o"></i></a></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        echo "<tr><td colspan='12'>No Result Found</td></tr>";
                                    endif;
                                    ?>
                                </tbody>								
                            </table>
							<button name="multi_delete" type="submit" class="viewDeleteButton delete-btn" id="delete-btn" value="multi_action">DELETE</button>
							<?php echo form_close();?>													
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>




