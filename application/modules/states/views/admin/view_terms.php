<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/states/view_terms' => "T&C's List")); ?>
    <div class="row border-bottom">

    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php echo print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group formWidht">
                                    <label>State</label>
                                    <?php
                                    $state_id = isset($_GET['state_id']) ? $_GET['state_id'] : '';
                                    state_dropdown($state_id, 'form-control');
                                    ?>                                     
                                </div>
                            </div>

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/states/view_terms'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title borderNone">
                        <h2>T&C's List </h2>
                        <div class="ibox-tools" style="display: inline-block; float: right;">  
                            <a href="<?php echo base_url('admin/states/add_terms'); ?>" class="btn btn-primary block full-width m-b createuser">ADD T&C</a>
                        </div>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>                                        
                                        <th>State</th>
                                        <th>T&C</th>
                                        <th>Is Retailer</th>                                        
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <?php
                                    if (!empty($records)):
                                        foreach ($records as $record):
                                            ?>
                                            <tr>                                                
                                                <td><?php echo $record->state; ?></td>
                                                <td><?php echo read_more($record->content); ?></td>
                                                <td><?php echo ($record->is_retailer == 1) ? 'YES':'NO'; ?></td>                                                                                                                                      
                                                <td><a href="<?php echo base_url('admin/states/edit_terms/') . $record->id; ?>"><i class="fa fa-pencil-square-o"></i></a></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        echo "<tr><td colspan='12'>No Result Found</td></tr>";
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>




