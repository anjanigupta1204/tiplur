<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/states/view/' . $states->id => 'View State')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <div class="tab-content">
                        <div class="row">                            
                            <div class="col-lg-12 colMargin">
                                <div class="panel-body">
                                    <div class="col-lg-6">
                                        <h4 class="orderListHeadsub">State Name : <?php echo isset($states->name) ? ucfirst($states->name) : 'NA'; ?></h2>
                                            <h4 class="orderListHeadsub">Allowed Age Limit: <?php echo isset($states->age) ? $states->age : 'NA'; ?></h2>
                                                </div>
                                                <div class="col-lg-6">    
                                                    <h4 class="orderListHeadsub">Opening Time : <?php echo isset($states->opening_time) ? $states->opening_time : 'NA'; ?></h2>
                                                        <h4 class="orderListHeadsub">Closing Time : <?php echo isset($states->closing_time) ? $states->closing_time : 'NA'; ?></h2>
                                                            </div>
                                                            </div>
                                                            </div>

                                                            <div class="col-lg-12 colMargin">
                                                                <div class="panel-body">
                                                                    <h2 class="orderListHead">Holidays Information</h2>
                                                                    <div class="ibox float-e-margins marginTbl">
                                                                        <div class="ibox-content orderBorderTop">
                                                                            <table class="table tblBottomMargin">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>S.No.</th>
                                                                                        <th>Title</th>
                                                                                        <th>Date</th>

                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                    $hdate = explode('<@>', $states->holiday_date);
                                                                                    $htitle = explode('<@>', $states->holiday_title);
                                                                                    if (!empty($hdate)):
                                                                                        $i = 1;
                                                                                        foreach ($hdate as $key => $val):
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td><?php echo $i++; ?></td>
                                                                                                <td><?php echo isset($val) ? $val : 'NA'; ?></td>
                                                                                                <td><?php
                                                                                                    if (!empty($htitle)) :
                                                                                                        echo $htitle[$key];
                                                                                                    else: echo "NA";
                                                                                                    endif;
                                                                                                    ?></td>

                                                                                            </tr>
                                                                                            <?php
                                                                                        endforeach;
                                                                                    else:
                                                                                        echo '<tr><td colspan="12">Result not found.</td></tr>';
                                                                                    endif;
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            </div>
