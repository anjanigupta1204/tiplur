<?php

class States_model extends CI_Model {

    protected $projectTable = "social_users";

    function __construct() {
        parent::__construct();
    }

    public function get_form_validation_rules() {

        $this->is_edit = ($this->uri->segment(3) == 'edit') ? true : false;

        $is_unique = !$this->is_edit ? '|is_unique[states.name]' : '';

        $form_validation = array(
            array(
                'field' => 'name',
                'label' => 'State Name',
                'rules' => 'required' . $is_unique
            ),
            array(
                'field' => 'age',
                'label' => 'age',
                'rules' => 'required|numeric'
            ),
            array(
                'field' => 'opening_time',
                'label' => 'opening time',
                'rules' => 'required'
            ),
            array(
                'field' => 'closing_time',
                'label' => 'closing time',
                'rules' => 'required'
            ),
            array(
                'field' => 'status',
                'label' => 'status',
                'rules' => 'required'
            )
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

    public function get_holiday_validation_rules() {
        //die("dfdffd");
        $form_validation = array(
            array(
                'field' => 'name',
                'label' => 'Holiday',
                'rules' => 'required'
            ),
            array(
                'field' => 'state_id',
                'label' => 'State',
                'rules' => 'required'
            ),
            array(
                'field' => 'date',
                'label' => 'Holiday Date',
                'rules' => 'required'
            ),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

    public function get_terms_validation_rules() {
        //die("dfdffd");
        $form_validation = array(
            array(
                'field' => 'is_retailer',
                'label' => 'Is Retailer',
                'rules' => 'required'
            ),
            array(
                'field' => 'state_id',
                'label' => 'State',
                'rules' => 'required'
            ),
            array(
                'field' => 'content',
                'label' => "T&C's",
                'rules' => 'required'
            ),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

    public function number_check($num) {
        //echo $str;die;
        if ($num <= 2) {
            return TRUE;
        }
        return FALSE;
    }

    public function state_details($filter = null, $limit = null, $offset = null, $count = true) {

        if (isset($filter['title']) and $filter['title']) {
            $this->db->like("name", trim($filter['title']));
        }

        if (isset($filter['age']) and $filter['age']) {
            $this->db->where("age", $filter['age']);
        }

        if (isset($filter['status']) and $filter['status'] != '') {
            $this->db->where("status", $filter['status']);
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        $this->db->order_by('name', 'ASC');
        $res = $this->db->get('states_detail');
        if ($count) {
            return $res->num_rows();
        } else {
            return $res->result();
        }
    }

    public function insert_state($data) {
        $this->db->insert('states', $data);
        $res = $this->db->insert_id();
        return ($res) ? $res : false;
    }
	
	public function delete_holiday($id){
		
		$return = $this->db->where('id',$id)->delete('state_holidays');
		return $return;
	}

    public function state_detail_by_id($id) {
        $res = $this->db->where('id', $id)->get('states_detail')->row();
        return ($res) ? $res : false;
    }

    public function terms_detail_by_id($id) {
        $res = $this->db->where('id', $id)->get('terms_n_conditions')->row();
        return ($res) ? $res : false;
    }

    public function check_terms_detail($state_id, $type, $id = false) {

        if (!empty($id)) {
            $this->db->where('id!=', $id);
        }

        $res = $this->db->where(array('state_id' => $state_id, 'is_retailer' => $type))->get('terms_n_conditions')->row();
        //echo $this->db->last_query();exit;
        return ($res) ? $res : false;
    }

    public function save_state_holidays($state_id, $data) {
        $h_ids = (!empty($data['hid'])) ? $data['hid'] : array();

        /* delete all the holidays if value is empty */
        if (!$data['holiday_date']) {
            $del_all = $this->db->where('state_id', $state_id)->delete('state_holidays');
        }

        /* delete all holidays which is not present in submit form data */
        if ($h_ids) {

            $del_res = $this->db->where_not_in('id', $h_ids)->where('state_id', $state_id)->delete('state_holidays');
        }

        // prd($data);
        for ($i = 0; $i < count($data['holiday_date']); $i++) {

            $db_data['title'] = $data['title'][$i];
            $db_data['holiday_date'] = $data['holiday_date'][$i];
            //update data
            if (!empty($data['hid'][$i])) {
                $db_data['modified_on'] = db_date_time();
                $this->db->where('id', $data['hid'][$i])->update('state_holidays', $db_data);
            } else {
                //insert data
                $db_data['created_on'] = db_date_time();
                $db_data['state_id'] = $state_id;
                $this->db->insert('state_holidays', $db_data);
            }
        }
    }

    public function holidays_listing($filter = null, $limit = null, $offset = null, $count = true) {

        if (isset($filter['state_id']) and $filter['state_id']) {
            $this->db->where("state_holidays.state_id", trim($filter['state_id']));
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        $this->db->select('state_holidays.*,states.name as state');
        $this->db->join('states', 'states.id=state_holidays.state_id', 'left');
        $res = $this->db->get('state_holidays');

        if ($count) {
            return $res->num_rows();
        } else {
            //echo $this->db->last_query(); die;
            return $res->result();
        }
    }

    public function terms_listing($filter = null, $limit = null, $offset = null, $count = true) {

        if (isset($filter['state_id']) and $filter['state_id']) {
            $this->db->where("terms_n_conditions.state_id", trim($filter['state_id']));
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        $this->db->select('terms_n_conditions.*,states.name as state');
        $this->db->join('states', 'states.id=terms_n_conditions.state_id', 'left');
        $res = $this->db->get('terms_n_conditions');

        if ($count) {
            return $res->num_rows();
        } else {
            //echo $this->db->last_query(); die;
            return $res->result();
        }
    }

}
