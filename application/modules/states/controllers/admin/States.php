<?php

class States extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('states_model');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->set_data = array('theme' => 'admin'); //define theme name 
        if (!$this->auth->_is_logged_in()) {
            redirect(base_url());
        }
    }

    /*  filter */

    function filter() {
        return $this->input->get(array('title', 'age', 'status'));
    }

    /*  Script for  product listing */

    function index() {
		
		$this->load->library('pagination');

        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);
        $site_url = !$query_string ? base_url(SITE_AREA . '/states') : base_url(SITE_AREA . '/states?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }
        $num_rows = $this->states_model->state_details($this->filter());
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);


        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $getting = $this->states_model->state_details($this->filter(), $limit, $offset, false);
        $data['result'] = $records;
        $data['js'] = array('/assets/js/state.js', '/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js');
        $this->theme($data);
    }

    function create() {

        if (isset($_POST['save'])) {

            $this->form_validation->set_rules($this->states_model->get_form_validation_rules());
            //
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter state details.');
            } else {
                if ($this->save_states()) {
                    set_flash_message($type = 'success', $message = 'State added successfully.');

                    redirect('admin/states');
                }
            }
        }

        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
        $data['js'] = array('/assets/js/state.js', '/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js');
        $this->theme($data);
    }

    function edit() {
        $id = $this->uri->segment(4);

        if (isset($_POST['save'])) {
            $this->form_validation->set_rules($this->states_model->get_form_validation_rules());
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter states details.');
            } else {
                $res = $this->update_states($id);
                if ($res) {
                    set_flash_message($type = 'success', $message = 'States Update successfully.');
                    redirect('admin/states');
                }
            }
        }

        $data['result'] = $this->states_model->state_detail_by_id($id);
        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
        $data['js'] = array('/assets/js/state.js', '/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js');

        $this->theme($data);
    }

    private function update_states($id = false) {
        $data = $this->input->post(array('age', 'opening_time', 'closing_time'));
        $data['status'] = (!empty($_POST['status']) && $_POST['status'] == 1) ? $_POST['status'] : '0';
        $data['modified_on'] = db_date_time();
        $this->db->where('id', $id);
        $update = $this->db->update('states', $data);
        if ($update) {
            /* state holidays add and update */
            $holiday_data = $this->input->post(array('holiday_date', 'title', 'hid'));
            //prd($holiday_data);            
            $holiday_date_arr = array_filter($holiday_data['holiday_date']);
            $holiday_title_arr = array_filter($holiday_data['title']);

            $this->states_model->save_state_holidays($id, $holiday_data);
            
            return $update ? $update : false;
        }
    }

    function upload_doc() {
        global $user;
        $user_id = $user->id;
        $path = "assets/uploads/states/";
        $location = FCPATH . $path . $user_id;

        /* if creating new event and first attemp then delete old */
        $check_uploded = $this->input->post('check_uploded');
        if (($check_uploded == 'no') and is_dir($location)) {
            $files = array_diff(scandir($location), array('.', '..'));
            foreach ($files as $file) {
                (is_dir("$location/$file") && !is_link($location)) ? delTree("$location/$file") : unlink("$location/$file");
            }
            rmdir($location);
        }


        /* if not exist then create dir */
        if (!is_dir($location)) {
            mkdir($location, 0777, true);
        }

        /* upload multi files */

        $ajax_data['file'] = $ajax_data['error'] = array();
        foreach ($_FILES as $file) {

            /* check file size */
            if ($file['size'] > 2048000) {
                $ajax_data['error'][] = '"' . $file['name'] . '" size is more than 2MB not allowed!';
                continue;
            }

            /*  check alredy uploded */
            if ($this->check_already_exist($location, $file['name'])) {
                $ajax_data['error'][] = '"' . $file['name'] . '" already uploaded';
                continue;
            }

            /* upload file */
            $file_name = time() . '@__@' . $file['name'];
            move_uploaded_file($file['tmp_name'], $location . '/' . $file_name);
            $ajax_data['file'][] = base_url($path . $user_id . '/' . $file_name);
            $ajax_data['file_name'][] = $file_name;
            $ajax_data['file_path'][] = str_replace('\\', '/', $location) . '/' . $file_name;
        }


        echo json_encode($ajax_data);
    }

    function check_already_exist($dir, $file) {
        $files = array_diff(scandir($dir), array('.', '..'));
        $files = array_map(function($name) {
            return explode('@__@', $name)[1];
        }, $files);
        return in_array($file, $files) ? true : false;
    }

    function delete_pro_img() {
        global $user;
        $item = $this->input->post('file_id');
        $user_id = $user->id;
        $dir = FCPATH . "assets/uploads/states/";
        list($type, $file) = explode('@_@', $item);
        /* uploaded delete */
        if ($type != 'temp') {
            $this->db->delete('files', array('name' => $file));
            if (file_exists($dir . $type . '/' . $file)) {
                unlink($dir . $type . '/' . $file);
            }
        }

        /* temp delete */ elseif (file_exists($dir . $user_id . '/' . $file)) {
            unlink($dir . $user_id . '/' . $file);
        }
    }

    private function move_pro_files($states_id) {
        global $user;
        $user_id = $user->id;
        $dir = FCPATH . "assets/uploads/states/";
        $files = array_diff(scandir($dir . $user_id), array('.', '..'));

        /* if first time then make pro_id dir */
        if (!is_dir($dir . $states_id)) {
            mkdir($dir . $states_id);
        }

        foreach ($files as $file) {
            rename($dir . $user_id . '/' . $file, $dir . $states_id . '/' . $file);
        }

        /* remove user id dir */
        rmdir($dir . $user_id);


        return $files;
    }

    //******** ajax function  ***************\\
    function image_slider_data() {
        $id = $this->input->post('id');
        $data['states'] = $this->states_model->state_detail_by_id($id);
        $html = $this->load->view('states/admin/slider', $data, true);
        echo $html;
    }

    function view($id) {
        $data['states'] = $this->states_model->state_detail_by_id($id);
        $this->theme($data);
    }

    function add_holiday() {

        if (isset($_POST['save'])) {

            $this->form_validation->set_rules($this->states_model->get_holiday_validation_rules());
            //
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter all required details.');
            } else {
                if ($this->save_holiday_data()) {
                    set_flash_message($type = 'success', $message = 'Holiday added successfully.');
                    redirect('admin/states/add_holiday');
                }
            }
        }

        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js', '/assets/js/state.js');

        $this->theme($data);
    }

    private function save_holiday_data($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        $return = false;
        if ($type == 'insert') {
            $data = array();
            $data['state_id'] = $this->input->post('state_id');
            $data['title'] = $this->input->post('name');
            $data['holiday_date'] = $this->input->post('date');
            $data['description'] = $this->input->post('description');
            $data['created_on'] = $data['modified_on'] = db_date_time();

            $this->db->insert('state_holidays', $data);

            $id = $this->db->insert_id();
            if (is_numeric($id)) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            $data['state_id'] = $this->input->post('state_id');
            $data['title'] = $this->input->post('name');
            $data['date'] = $this->input->post('date');
            $data['description'] = $this->input->post('description');
            $data['modified_on'] = db_date_time();
            $return = $this->db->where('id', $id)->update('state_holidays', $data);
        }

        return $return;
    }

    public function view_holidays() {

		// delete multiple holiday
        if (!empty($_GET['multi_delete'])) {			
            $this->delete_holidays();
        }
	
        $this->load->library('pagination');

        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/states/view_holidays') : base_url(SITE_AREA . '/states/view_holidays?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }

        $num_rows = $this->states_model->holidays_listing($this->holiday_filter());
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $this->states_model->holidays_listing($this->holiday_filter(), $limit, $offset, false);
        $data['records'] = $records;
        //prd($records);
        $this->theme($data);
    }

    function holiday_filter() {
        return $this->input->get(array('state_id'));
    }

    function add_terms() {

        if (isset($_POST['save'])) {

            $this->form_validation->set_rules($this->states_model->get_terms_validation_rules());
            //
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter all required details.');
            } else {
                if ($this->save_terms_data()) {
                    set_flash_message($type = 'success', $message = 'Terms and Condition added successfully.');
                    redirect('admin/states/add_terms');
                }
            }
        }

        $data['css'] = array('/theme/admin/css/bootstrap-datetimepicker.css');
        $data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js','/theme/admin/js/ckeditor/ckeditor.js', '/assets/js/state.js');

        $this->theme($data);
    }

    function edit_terms() {
        $id = $this->uri->segment(4);

        if (isset($_POST['save'])) {
            //prd($_POST);
            $this->form_validation->set_rules($this->states_model->get_terms_validation_rules());
            if ($this->form_validation->run() === false) {
                set_flash_message($type = 'error', $message = 'Please enter all required details.');
            } else {
                if ($this->save_terms_data('update', $id)) {
                    set_flash_message($type = 'success', $message = 'Updated successfully.');
                    if (empty($this->uri->segment(4))) {
                        redirect('admin/states/edit_terms');
                    } else {
                        redirect('admin/states/view_terms');
                    }
                }
            }
        }

        $data['result'] = $this->states_model->terms_detail_by_id($id);
		$data['js'] = array('/theme/admin/js/moment.js', '/theme/admin/js/bootstrap-datetimepicker.js','/theme/admin/js/ckeditor/ckeditor.js', '/assets/js/state.js');

        $this->theme($data);
    }

    private function save_terms_data($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }
        
        $return = false;
        if ($type == 'insert') {
            $data = array();
            $data['state_id'] = $this->input->post('state_id');
            $data['content'] = $this->input->post('content');
            $data['is_retailer'] = ($this->input->post('is_retailer') == 1) ? $this->input->post('is_retailer') : 0;
            $data['created_date'] = $data['modified_date'] = db_date_time();

            $is_exist = $this->states_model->check_terms_detail($data['state_id'], $data['is_retailer']);

            if ($is_exist) {
                set_flash_message($type = 'error', $message = 'T&C already exist for this state.');
                redirect('admin/states/add_terms');
            }

            $this->db->insert('terms_n_conditions', $data);

            $id = $this->db->insert_id();
            if (is_numeric($id)) {
                $return = $id;
            }
        } elseif ($type == 'update') {
            $data['state_id'] = $this->input->post('state_id');
            $data['content'] = $this->input->post('content');
            $data['is_retailer'] = $this->input->post('is_retailer');
            $data['modified_date'] = db_date_time();

            $is_exist = $this->states_model->check_terms_detail($data['state_id'], $data['is_retailer'], $id);

            if ($is_exist) {
                set_flash_message($type = 'error', $message = 'T&C already exist for this state.');
                redirect('admin/states/edit_terms/' . $id);
            }

            $return = $this->db->where('id', $id)->update('terms_n_conditions', $data);
        }

        return $return;
    }

    public function view_terms() {

        $this->load->library('pagination');

        $per_page = isset($_GET['per_page']) ? $_GET['per_page'] : false;
        unset($_GET['per_page']);
        $query_string = http_build_query($_GET);

        $site_url = !$query_string ? base_url(SITE_AREA . '/states/view_terms') : base_url(SITE_AREA . '/states/view_terms?' . $query_string);
        if ($per_page) {
            $_GET['per_page'] = $per_page;
        }

        $num_rows = $this->states_model->terms_listing($this->holiday_filter());
        $config = pagination_formatting();
        $limit = 10;
        $config['base_url'] = $site_url;
        $config['total_rows'] = $num_rows;
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $offset = isset($_GET['per_page']) ? ($_GET['per_page'] - 1) * $limit : null;
        $records = $this->states_model->terms_listing($this->holiday_filter(), $limit, $offset, false);
        $data['records'] = $records;
        //prd($records);
        $this->theme($data);
    }

    private function save_states() {

        $data = $this->input->post(array('name', 'age', 'opening_time', 'closing_time', 'status'));
        $data['modified_on'] = db_date_time();

        $id = $this->states_model->insert_state($data);

        return $id;
    }
	
	function delete_holidays() {

        $checked = $this->input->get('holiday_id');

        if (is_array($checked) && count($checked)) {
            // If any of the deletions fail, set the result to false, so
            // failure message is set if any of the attempts fail, not just
            // the last attempt 

            $result = true;
            foreach ($checked as $pid) {

                $updated = $this->states_model->delete_holiday($pid);
                if ($updated == false) {
                    $result = false;
                }
            }
            if ($result) {
                set_flash_message($type = 'success', $message = count($checked) . ' row deleted successfully.');
                redirect('admin/states/view_holidays');
            } else {
                set_flash_message($type = 'error', $message = 'something wrong');
                redirect('admin/states/view_holidays');
            }
        } else {
            set_flash_message($type = 'error', $message = 'Please select product.');
            redirect('admin/states/view_holidays');
        }
    }

}
