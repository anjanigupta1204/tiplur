<?php

class Users extends MX_Controller{ 

    function __construct() {
        parent::__construct();
		$this->load->library('auth');
		$this->load->model('user_model');
		$this->load->library('form_validation');
    } 
	

    
    function index() {
			$this->theme();
	}
	
    function login() {  
	    // If alredy login
        if ($this->auth->_is_logged_in()){
            $this->auth->check_login();
        }
		
		/*if form submit request*/
		if(isset($_POST['login']) and $this->user_model->get_login_validation_rules()){  
	
			$this->auth->auth_login($this->input->post('email'),$this->input->post('password'),1); 
		}
		
		$data['temp'] = 'login'; 
		$this->theme($data);
    } 
	

    function logout()
	{ 
		//prd($this->session->userdata());
		$userdata = $this->session->userdata(); 
		$this->session->unset_userdata($userdata);
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
	

    function forgot_password(){
		 $email = $this->input->post('email'); 
		 $result = $this->user_model->validate_email($email);
		 if($result)
		 {
		   $data['status'] = $result;
		 }
		 echo json_encode($data);
	}
	
	
	function reset_password_frontend()
	{  
		 if($this->input->post('token'))
		 {
			$token  = $this->input->post('token');
		 }
		 else if($this->input->get('token'))
		 {
			 $token  = $this->input->get('token');
		 }else
		 {
			 $token  = '';
		 }
		 
		   
		
		if($token)
		{
			$result  = $this->user_model->validate_token($token); 
			if(empty($result))
			{
				$this->session->set_flashdata('message', 'Your reset password link expired. Please try again');
			}else
			{  
				$email = $result ? $result: '';
				if (!empty($_POST['reset_pass'])) 
				{   
					$data = array('password'  => md5($this->input->post('password')),'reset_time'=> 0,'reset_hash'=> '');
					$update_result = $this->db->where('email',$email)->update('users',$data);
					if($update_result)
					{
						$this->session->set_flashdata('message','Your Password changed successfully,Please login using your new password.');
						redirect(base_url('login')); 
					}
					
				
				}
			}
		}else{
			$this->session->set_flashdata('message','Reset Password Token is missing');
		}
		
		$data['temp'] = 'reset_password'; 
		$this->theme($data);
	}
	
	
	
	
    
}
