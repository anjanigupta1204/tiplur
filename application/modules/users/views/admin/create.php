<div id="page-wrapper" class="gray-bg dashbard-1">
		<?php breadcrumbs(array('admin/users'=>'User Management','admin/users/create'=>'Create User'));  ?>

            <div class="row border-bottom">
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
						<?php print_flash_message(); ?>
                    <div class="col-lg-12">
					<?php echo  form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"','autocomplete="off"'); ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h2 style="display: inline-block;">Create User</h2>                               
                            </div>
                            <div class="ibox-content contentBorder">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                            <div class="form-group formWidht">
                                                <label>Name <span style="color: red">*</span></label>
                                                <input type="text" placeholder="First Name" required name="display_name"  value="<?php echo set_value('display_name', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                            </div>
											<span class='error vlError'><?php echo form_error('display_name'); ?></span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                            <div class="form-group formWidht">
                                                <label>Phone Number <span style="color: red">*</span></label>
                                                <input type="mobile" placeholder="Phone Number" required  name="mobile" value="<?php echo set_value('mobile', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                            </div>
											<span class='error vlError'><?php echo form_error('mobile'); ?></span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                            <div class="form-group formWidht">
                                                <label>Email ID <span style="color: red">*</span></label>
                                                <input type="email" placeholder="Email ID" name="email" required  value="<?php echo set_value('email', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                            </div>
											<span class='error vlError'><?php echo form_error('email'); ?></span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                            <div class="form-group formWidht">
                                                <label>Password <span style="color: red">*</span></label>
                                                <input type="password" placeholder="Password" name="password" required  id="exampleInputEmail2" value="<?php echo set_value('password', ''); ?>" class="form-control formWidht">
                                            </div>
											<span class='error vlError'><?php echo form_error('password'); ?></span>
                                    </div>
									 <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                            <div class="form-group formWidht">
                                                <label>Confirm Password <span style="color: red">*</span></label>
                                                <input type="password" placeholder="Confirm password" name="confirm_password" required  id="exampleInputEmail2" value="<?php echo set_value('confirm_password', ''); ?>" class="form-control formWidht">
                                            </div>
											<span class='error vlError'><?php echo form_error('confirm_password'); ?></span>
                                    </div>
									 <input type="hidden" name="status"   value="1"/>
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                            <div class="form-group formWidht">
                                                <label>Address <span style="color: red">*</span></label>
                                                <textarea placeholder="Address"  name="address" required  id="exampleInputEmail2" class="form-control formWidht"><?php echo set_value('address', ''); ?></textarea>
                                            </div>
											<span class='error vlError'><?php echo form_error('address'); ?></span>
                                    </div>                                    
                                    <div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont">
                                            <div class="form-group formWidht">
												<?php $role_id= (!empty($_POST['role_id']))?$_POST['role_id']:''; ?>
                                                <label>Role <span style="color: red">*</span></label>
                                                <select class="form-control m-b addContDrop" required  name="role_id" name="account">
                                                    <option value=" ">Select</option>
													<?php roles($role_id); ?>
                                                </select>
                                            </div>
											<span class='error vlError'><?php echo form_error('role_id'); ?></span>
                                    </div>
								<div class="col-lg-6 col-md-6 col-sm-6 AddProdctInputCont ">
									 <div class="form-group formWidht">
                                     <label>Profile Image <span style="color: white">*</span> </label>
										<input name="h_image" id="old_user_pic" value=""  class="form-control valid old_user_pic" readonly="" aria-invalid="false" type="text">
										<input name="image" value=""  type="hidden">
										<label class="input-group-btn crateAddImageButton">
											<span class="btn btn-primary">
												BROWSE <input name="image" id="user_pic" class="user_pic" accept="image/gif, image/jpg, image/jpeg, image/png"   type="file">
											</span>								
										</label>	
									</div> 
								</div>

                                <div class="col-lg-12 col-md-12 col-sm-12  text-center" style="margin-top:80px;">
                                        <input type="submit" name="save" value="CREATE USER" class="btn btn-primary block full-width m-b updateProductBtn"/>
                                    </div>
                                    <!--div class="col-lg-8 col-md-8 col-sm-8 AddProdctInputCont">
                                    <label>Upload</label>
                                        <div class="input-group"><input type="text" name="image" value="<?php echo set_value('image', ''); ?>" class="form-control"> 
											<div id="customfileupload">Select a file</div>
											<span class="input-group-btn">
											<input type="file" style="display:none;" id="upload_click"/>
											  <button type="button" class="btn btn-default browseBtnUpload" onclick="$('#upload_click').click({  $('#customfileupload').html($(this).val()); }); " >BROWSE </button> 
											</span>
										</div>
                                    </div-->       
                                    
                                </div>
                            </div>
                        </div>
						</form>
                    </div>
                </div>
            </div>
	 