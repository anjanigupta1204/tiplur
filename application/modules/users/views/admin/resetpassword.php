<div id="page-wrapper" class="gray-bg dashbard-1">
			<?php breadcrumbs(array('admin/users'=>'User Management','admin/users/resetpassword'=>'Reset Password'));  ?>
            <div class="row border-bottom">
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
						<?php if ($this->session->flashdata('message')) { ?>
							<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<?php echo $this->session->flashdata('message'); ?>
							</div>
						<?php } ?>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h2 style="display: inline-block;">Reset Password</h2>
                                
                            </div>
							<?php echo form_open($this->uri->uri_string(), array('class' => '', 'autocomplete' => 'off')); ?>
                            <div class="ibox-content contentBorder">
                                <div class="row">
                                    <div class="formCenter">
                                        
                                            <div class="form-group formWidht">
                                                <label>Old Password <span style="color: red;">*</span></label>
                                                <input type="password" placeholder="Old Password" id="old_pass" name="old_pass" class="form-control formWidht">
											</div>
                                        <span class='error vlError'><?php echo form_error('old_pass'); ?></span>
                                    </div>
									
								</div>
								<div class="row">
                                    <div class="formCenter">
                                        
                                            <div class="form-group formWidht">
                                                <label>New Password <span style="color: red;">*</span></label>
                                                <input type="password" placeholder="New Password" id="password" name="password" class="form-control formWidht">
											</div>
                                        	<span class='error vlError'><?php echo form_error('password'); ?></span>
                                    </div>
								
                                 </div>
								 <div class="row">
                                    <div class="formCenter">
                                        
                                            <div class="form-group formWidht">
                                                <label>Confirm Password <span style="color: red;">*</span></label>
                                                <input type="password" placeholder="Confirm Password" id="cpass" name="cpass" class="form-control formWidht">
												
                                            </div>
											<span class='error vlError'><?php echo form_error('cpass'); ?></span>
                                    </div>
									
                                 </div>
                                <div class="formCenter">
                                    <div class="text-right">
                                    <button type="submit" class="btn btn-primary block full-width m-b updateProfileBtn createuser">UPDATE</button>
                                    </div>
                        </div>
                            </div>
							<?php echo form_close(); ?>   
                        </div>
                    </div>
                </div>
            </div>
            
       