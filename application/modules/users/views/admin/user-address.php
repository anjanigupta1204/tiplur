<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?php echo (!empty($store_detail)) ? 'Store Detail' : 'User Address'; ?></h4>
</div>


<div class="modal-body">

    <?php if (!empty($store_detail)): ?>
        <table class="table table-striped table-bordered" border="1">
            <thead>
                <tr>
                    <th>License No.</th>
                    <th>Store Name</th>                
                    <th>Contact Person</th>
                    <th>Phone Number</th>
                    <th>Store Timing</th>
                    <th>Min Order</th>
                    <th>Provide Snacks</th>                
                </tr>
            </thead>                                
            <tbody>               
                <tr>
                    <td><?php echo $store_detail->licence; ?></td>
                    <td><?php echo $store_detail->store_name; ?></td>
                    <td><?php echo $store_detail->contact_person; ?></td>
                    <td><?php echo $store_detail->contact_person_mobile; ?></td>
                    <td><?php echo $store_detail->opening_time . "--" . $store_detail->closing_time; ?></td>
                    <td><?php echo $store_detail->min_order; ?></td>
                    <td ><?php echo ($store_detail->is_provide_snacks == 1) ? 'YES' : 'NO'; ?></td>
                </tr>                
            </tbody>                                                    
        </table> 
        <hr>
        <h4>Address Detail</h4>
    <?php endif; ?>    
    <ol>
        <?php foreach ($addresses as $address): ?>
            <li><strong><?php echo $address->address . "," . $address->city . "," . $address->state . "," . $address->country . "," . $address->pincode; ?></strong></li>
        <?php endforeach; ?>
    </ol>
</div>             
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
