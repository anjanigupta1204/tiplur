<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php breadcrumbs(array('admin/users' => 'User Management', 'admin/users/create' => 'Update User')); ?>			
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <?php print_flash_message(); ?>
            <div class="col-lg-12">
                <?php
                $type = (!empty($_GET['type'])) ? '/?type' . $_GET['type'] : '';
                echo form_open_multipart($this->uri->uri_string() . $type, 'class="form-horizontal"', 'autocomplete="off"');
                ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h2 style="display: inline-block;">Update User</h2>
                    </div>
                    <div class="ibox-content contentBorder">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form-group formWidht">
                                    <label>Name <span style="color: red;">*</span></label>
                                    <input type="text" placeholder="First Name" required name="display_name"  value="<?php echo set_value('display_name', $result->display_name); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                </div>
                                <span class='error vlError'><?php echo form_error('display_name'); ?></span>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form-group formWidht">
                                    <label>Phone Number <span style="color: red;">*</span></label>
                                    <input type="mobile" placeholder="Phone Number" required name="mobile" value="<?php echo set_value('mobile', $result->mobile); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                </div>
                                <span class='error vlError'><?php echo form_error('mobile'); ?></span>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="form-group formWidht">
                                    <label>Email ID <span style="color: red;">*</span></label>
                                    <input type="text" autocomplete="off" <?php echo (empty($this->uri->segment(4))) ? 'readonly="true" ' : ''; ?>required placeholder="Email ID" name="email" value="<?php echo set_value('email', $result->email); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                </div>
                                <span class='error vlError'><?php echo form_error('email'); ?></span>
                            </div>
                            <input type="hidden" name="status" value="1"/>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12" style="padding:0px;">
                                <div class="<?php echo (empty($this->uri->segment(4))) ? 'col-lg-12 col-md-12 col-sm-12' : 'col-lg-4 col-md-4 col-sm-4'; ?> textAreaAddProdctInputCont ">
                                    <div class="form-group formWidht">
                                        <label>Address <span style="color: red;">*</span></label>
                                        <textarea placeholder="Address"  name="address" required id="exampleInputEmail2" class="form-control formWidht"><?php echo set_value('address', @$result->address); ?></textarea>
                                    </div>
                                    <span class='error vlError'><?php echo form_error('address'); ?></span>
                                </div> 
                                <?php if (!empty($this->uri->segment(4))): ?>									
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                        <div class="form-group formWidht">
                                            <label>Role</label>
                                            <?php $type = (!empty($_GET['type'])) ? $_GET['type'] : ''; ?>
                                            <select class="form-control m-b addContDrop"<?php echo (!empty($_GET['type'])) ? 'disabled1' : ''; ?> required name="role_id" name="account">
                                                <option value=" ">Select</option>
                                                <?php roles($result->role_id); ?>
                                            </select>
                                        </div>
                                        <span class='error vlError'><?php echo form_error('role_id'); ?></span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                        <div class="form-group formWidht ">
                                            <label>Status   <span>*</span></label>
                                            <select name="status" required <?php echo (!empty($this->uri->segment(4))) ? '' : 'readonly'; ?> class="form-control m-b addContDrop product_category ">
                                                <?php
                                                $check = (isset($result->status) && $result->status == 1) ? $result->status : 2;
                                                $status = status();
                                                foreach ($status as $k => $val):
                                                    ?>
                                                    <option readonly value="<?php echo $k; ?>" <?php echo ($k == @$check) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                                <?php endforeach; ?>
                                                <span class='error vlError'><?php echo form_error('status'); ?></span>
                                            </select>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <input type="hidden" name="role_id" value="<?php echo $result->role_id; ?>" >
                                    <input type="hidden" name="status" value="<?php echo $result->status; ?>" >
                                <?php endif; ?>	
                            </div>

                            <div class="col-lg-8 col-md-8 col-sm-8 " style="height: 50px;">
                                <div class="form-group formWidht">
                                    <label>Profile Image </label>
                                    <input readonly="readonly" name="h_image" id="old_user_pic" value="<?php echo ($result->image) ? $result->image : ''; ?>" class="form-control valid old_user_pic" readonly="" aria-invalid="false" type="text">
                                    <input name="image" value="" type="hidden">
                                    <label  class="input-group-btn crateAddImageButtonuser TopEdit">
                                        <span class="btn btn-primary">
                                            BROWSE <input name="image" id="user_pic" class="user_pic"  accept="image/gif, image/jpg, image/jpeg, image/png" type="file">
                                        </span>								
                                    </label>	
                                    <?php if (!empty($result->image)): ?>
                                        <input id='h_image' type='hidden' name='h_image'  value="<?php echo ($result->image) ? $result->image : ''; ?>" />
                                        <span class='help-inline'><img class="browseImage" src="<?php echo img_src('/assets/images/profile_image/', $result->image); ?>" width="100"></span>
                                    <?php endif; ?>
                                </div> 
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4" style="height: 115px;"><div>									
                                </div></div>
                            <?php if (!empty($this->uri->segment(4))): ?>
                                <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                    <div class="form-group formWidht changePass" >
                                        <label>Change password : - 
                                            <input type="checkbox" id="change-password" name="check" value="1" <?php echo (isset($_POST['check']) && $_POST['check'] == 1) ? "checked" : ""; ?> >
                                        </label>
                                    </div>

                                </div>
                                <div id="form_add_password">
                                    <?php if ((!empty($_POST['password'])) || (!empty($_POST['confirm_password']))): ?>
                                        <div class="col-lg-4 col-md-4 col-sm-4 ">
                                            <div class="form-group formWidht">
                                                <input type="password" required placeholder="Password" name="password"  id="exampleInputEmail2" value="<?php echo set_value('password', ''); ?>" class="form-control formWidht">
                                            </div>
                                            <span class='error vlError'><?php echo form_error('password'); ?></span>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 ">
                                            <div class="form-group formWidht">
                                                <input type="password" placeholder="Confirm password" name="confirm_password"  id="exampleInputEmail2" value="<?php echo set_value('confirm_password', ''); ?>" class="form-control formWidht">
                                            </div>
                                            <span class='error vlError'><?php echo form_error('confirm_password'); ?></span>
                                        </div>
                                    <?php endif ?>
                                </div>
                            <?php endif; ?>
                            <div class="col-lg-12 col-md-12 col-sm-12  text-center">
                                <input type="submit" name="save" value="UPDATE PROFILE" class="btn btn-primary block full-width m-b updateProductBtn"/>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="for-password" style="display:none;" >
        <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
            <div class="form-group formWidht">
                <input type="password" required  autocomplete="off" placeholder="Password" name="password"  id="exampleInputEmail2" value="<?php echo set_value('password', ''); ?>" class="form-control formWidht">
            </div>
            <span class='error vlError'><?php echo form_error('password'); ?></span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
            <div class="form-group formWidht">
                <input type="password" placeholder="Confirm password" autocomplete="off" name="confirm_password"  id="exampleInputEmail2" value="<?php echo set_value('confirm_password', ''); ?>" class="form-control formWidht">
            </div>
            <span class='error vlError'><?php echo form_error('confirm_password'); ?></span>
        </div>
    </div>	