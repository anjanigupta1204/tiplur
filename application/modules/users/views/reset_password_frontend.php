<div class="middle-box text-center loginscreen animated fadeInDown">
	<div>
		<div>                
			<div class="loginLogo"></div>
		</div> 
		<?php if ($this->session->flashdata('message')) { ?>
				<div class="alert alert-error" style="background-color: blanchedalmond;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('message'); ?>
				</div>
	    <?php } else { ?>
		<?php echo form_open(base_url('reset-password'), array('class' => 'm-t loginLlbl', 'autocomplete' => 'off','id'=>'reset_validation','method'=>'post')); ?>
			<div class="form-group">   
				<label for="usr">New Password</label>
				<input type="password" name="password" id="password" class="form-control" placeholder="New Password" >
				<span class='error'><?php echo form_error('password'); ?></span>
			</div>
			<div class="form-group">
				<label for="pwd">Confirm Password</label>
				<input type="password" name="cpass" id="cpass" class="form-control" placeholder="Confirn Password" >
				<span class='error'><?php echo form_error('cpass'); ?></span>
			</div>
			<input type="hidden"  id="token"  name="token"   value="<?php echo $this->input->get('token') ? $this->input->get('token') :'' ?>" />
			<input type="submit" class="btn btn-primary block full-width m-b loginBtn"  name="reset_pass"  required=""> 
			
		
		<?php echo form_close(); ?>     
		<?php  }  ?>		
	</div>
</div>


