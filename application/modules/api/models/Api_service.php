<?php

/**
 *  * this service class is responsible of all the application logic 
 * related to API
 */
class Api_service extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('api/Api_dao');
        include_once './application/objects/Response.php';
    }

    //states
    public function State_list() {
        $response = new response ();
        try {
            $result = $this->db->select('id,name,age,opening_time,closing_time,is_available')->where('status', '1')->order_by('name', 'ASC')->get('states')->result();

            if ($result) {
                $response->setStatus(1);
                $response->setMsg('Success');
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    //registration
    public function registration_process() {
        $Returndata = array();
        $response = new Response ();
        try {
            $post = $this->input->post();
            $apiDao = new Api_dao ();
            $isUserExist = $apiDao->isUserExist($post);

            if ($isUserExist->getStatus() == 1) {
                $user = $isUserExist->getObjArray();
                if ($user->status == '1') {
                    $Returndata ['id'] = $user->id;
                    $Returndata ['role_id'] = $user->role_id;
                    $Returndata ['email'] = $user->email;
                    $Returndata ['mobile'] = $user->mobile;
                    $Returndata ['username'] = $user->display_name;
                    $Returndata ['address'] = $user->address;
                    $Returndata ['dob'] = $user->dob;
                    $Returndata ['doc_file'] = ($user->doc_file != '') ? base_url() . 'assets/uploads/document_file/' . $user->doc_file : '';
                    $Returndata ['image'] = ($user->image != '') ? base_url() . 'assets/images/profile_image/' . $user->image : '';
                    $Returndata ['store'] = $user->store;

                    $updateDeviceData = $apiDao->updateDeviceData($user->id, $post['device_type'], $post['device_id'], $post['fcm_reg_id']);

                    if ($updateDeviceData) {
                        $response->setStatus(1);
                        $response->setMsg("Success");
                        $response->setObjArray($Returndata);
                    } else {
                        $response->setStatus(0);
                        $response->setMsg("Something went wrong");
                        $response->setObjArray('');
                    }
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Inactive user");
                    $response->setObjArray('');
                }
            } else {
                $response->setStatus(0);
                $response->setMsg($isUserExist->getMsg());
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }

        return $response;
    }

    //Login api
    public function login() {
        $ReturnData = array();
        $postData = array();
        $response = new Response ();
        try {
            $post = $this->input->post();
            $apiDao = new Api_dao ();
            $checkUser = $this->validateUser($post['username'], $post['password'], $post['login_type']);
            if ($checkUser['status'] == '1') {
                $isUserExist = $checkUser['data'];
                if ($isUserExist->status == '1') {
                    //$randcode = substr(rand(1, 99999), 1, 5);
                    $is_default = 1;
                    $ReturnData ['id'] = $isUserExist->id;
                    $ReturnData ['role_id'] = $isUserExist->role_id;
                    $ReturnData ['email'] = $isUserExist->email;
                    $ReturnData ['mobile'] = $isUserExist->mobile;
                    $ReturnData ['username'] = $isUserExist->display_name;
                    $ReturnData ['dob'] = $isUserExist->dob;
                    $terms = $this->is_terms_accepted($ReturnData ['id'], $isUserExist->state_id);
                    $ReturnData ['is_terms_accepted'] = $terms != NULL ? $terms[0]->is_accepted : '0';
                    $ReturnData ['doc_file'] = ($isUserExist->doc_file != '') ? base_url() . 'assets/uploads/document_file/' . $isUserExist->doc_file : '';
                    $ReturnData ['image'] = ($isUserExist->image != '') ? base_url() . 'assets/images/profile_image/' . $isUserExist->image : '';
                    $get_default_address = $apiDao->get_address($ReturnData ['id'], $is_default);
                    if ($get_default_address) {
                        $ReturnData['address'] = $get_default_address[0];
                        $ReturnData['address']->state_id = $isUserExist->state_id;
                    } else {
                        $ReturnData['address'] = NULL;
                    }

                    if (!$post['is_retailer']) { //if customer
                        $cart['user_id'] = $ReturnData ['id'];

                        $getCart = $apiDao->get_cart($cart);

                        $cartCount = 0;
                        $retailer_id = NULL;
                        if (sizeof($getCart) > 0) {
                            $liquorCount = sizeof($getCart['liquor']);
                            $snacksCount = sizeof($getCart['snacks']);

                            $carts['liquor'] = $getCart['liquor'];
                            $carts['snacks'] = $getCart['snacks'];

                            foreach ($carts as $val) {

                                $productCount = sizeof($val);
                                $cartCount = intval($cartCount + $productCount);

                                if ($productCount > 0) {
                                    $retailer_id = $val[0]['retailer_id'];
                                }
                            }
                            /* echo "Retailer Id " .  $retailer_id;
                              echo "<br>"; */
                            $retailer = user_data($retailer_id);
                            //print_r($retailer);die;
                            $store['user_id'] = $retailer_id;
                            $getStore = $apiDao->get_store($store);
                            //$cartData['total_item'] = $cartCount;
                            $cartData['total_item'] = array('liquor' => $liquorCount, 'snacks' => $snacksCount);
                            $cartData['retailer'] = array("id" => $retailer->id, "name" => $retailer->display_name, "store" => $getStore->store_name);
                        }
                        $ReturnData ['cart'] = $cartCount != 0 ? $cartData : "";
                    } else { //if retailer
                        $store['user_id'] = $ReturnData ['id'];

                        $getStore = $apiDao->get_store($store);
                        $ReturnData ['store'] = sizeof($getStore) > 0 ? $getStore : "";
                        $data = array();
                        $data['retailer_id'] = $ReturnData ['id'];
                        $data['order_id'] = '0';
                        $ReturnData ['order_count'] = sizeof($apiDao->requested_orders($data));
                    }




                    $updateDeviceData = $apiDao->updateDeviceData($isUserExist->id, $post['device_type'], $post['device_id'], $post['fcm_reg_id']);
                    if ($updateDeviceData) {
                        $response->setStatus(1);
                        $response->setMsg("Valid user");
                        $response->setObjArray($ReturnData);
                    } else {
                        $response->setStatus(0);
                        $response->setMsg("Database error");
                        $response->setObjArray('');
                    }
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Inactive user");
                    $response->setObjArray('');
                }
            } else if ($checkUser['status'] == '2') {
                $response->setStatus($checkUser['status']);
                $response->setMsg($checkUser['msg']);
                $response->setObjArray('');
            } else {
                $response->setStatus(0);
                $response->setMsg("username and password combination is wrong.");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }

        return $response;
    }

    //check user exist
    public function isUserExist($unique_id, $type) {
        $response = new Response ();
        try {
            $apiDao = new Api_dao ();
            $response = $apiDao->isUserExist($unique_id, $type);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    //Validate user
    public function validateUser($username, $password, $login_type) {

        $response = array();

        $this->db->where('mobile', $username);
        $this->db->or_where('email', $username);
        $is_user_exist = $this->db->get('users');
        if ($is_user_exist->num_rows() > 0) {
            $this->db->where('mobile', $username);
            $this->db->or_where('email', $username);
            if ($login_type == LOGIN_BY_PASSWORD) {
                $this->db->where('password', md5($password));
            }

            $result = $this->db->get('users');
            if ($result->num_rows() > 0) {
                $user_id = $result->row()->id;
                $query = user_data($user_id);
                $response['status'] = '1';
                $response['msg'] = 'User exist.';
                $response['data'] = $query;
            } else {
                $response['status'] = '0';
                $response['msg'] = 'Invalid credentials.';
                $response['data'] = '';
            }
        } else {
            $response['status'] = '2';
            $response['msg'] = 'You are not a registered user.';
            $response['data'] = '';
        }
        return $response;
    }

    //update users detail 
    public function update_user_profile() {
        $response = new response ();
        try {

            $data = $this->input->post();
            $apiDao = new Api_dao ();

            $response = $apiDao->update_profile($data);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function search_retailers() {
        $response = new response ();
        try {
            $data = $this->input->post();
            $apiDao = new Api_dao ();

            $is_available = $apiDao->is_available_in_state($data['state_id']);
            if ($is_available) {
                $ids = NULL;
                if ($data['user_id'] != '0') {
                    $get_blocked_retailers = $apiDao->is_order_declined($data['user_id']);
                    if ($get_blocked_retailers) {
                        $retailers = array();
                        foreach ($get_blocked_retailers as $val) {

                            array_push($retailers, $val->retailer_id);
                        }

                        $ids = join("','", $retailers);
                    }
                }

                //print_r($ids);die;
                $res = $apiDao->find_reatilers($data, $ids);
                if ($res) {
                    $response->setStatus(1);
                    $response->setMsg('success');
                    $response->setObjArray($res);
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Not Found");
                    $response->setObjArray('');
                }
            } else {
                $response->setStatus(0);
                $response->setMsg("Thanks for being on tiplur, as of now our services are not available here.");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function all_categories() {
        $response = new response ();
        try {
            $apiDao = new Api_dao ();
            $res = $apiDao->get_all_categories();
            if ($res) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function subcategories_list() {
        $response = new response ();
        try {
            //$category_id = $this->input->post('category_id');
            //$this->load->model('Category/category_model');
            //$res = $this->category_model->subcategory_listing($category_id);
            $data = $this->input->post();
            $apiDao = new Api_dao ();
            $res = $apiDao->subcategory_listing($data);
            if ($res) {
                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($res);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /* validate mobile number */

    public function validate_mobile() {
        $response = new response ();
        try {
            //When user trying to login
            if ($this->input->post('is_login') == 1) {
                $mobile = $this->input->post('mobile');

                $randcode = rand(10000, 99999);
                $postData['otp'] = $randcode;

                /*
                 * Send otp
                 */
                $sender_id = 'TIPLUR';
                $user_id = urlencode('rakshat.chopra@gmail.com');
                $pwd = urlencode('RC@123tm');
                $message = urlencode("Dear Customer, Your OTP is " . $randcode . " Have a pleasant day!");

                $URL = "http://49.50.77.216/API/SMSHttp.aspx?UserId=" . $user_id . "&pwd=" . $pwd . "&Message=" . $message . "&Contacts=" . $mobile . "&SenderId=" . $sender_id . "";
                $ch = curl_init($URL);
                curl_setopt($ch, CURLOPT_URL, $URL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);

                /* print_r($mobile);
                  print_r("<br>");
                  print_r($result);
                  die; */
                curl_close($ch);

                $response->setStatus(1);
                $response->setMsg("Valid Mobile Number");
                $response->setObjArray($postData);
            } else { //When user trying to register
                /* $mobile = $this->input->post('mobile');
                  $query = $this->db->select('id,mobile')->where('mobile',$mobile)->get('users')->row();
                  if($query)
                  {
                  $response->setStatus ( 0 );
                  $response->setMsg ( "Mobile number already registered." );
                  $response->setObjArray ( NULL );

                  }else{

                  $randcode = rand(10000, 99999);
                  $postData['otp'] = $randcode;

                  $response->setStatus ( 1 );
                  $response->setMsg ( "User is not registered." );
                  $response->setObjArray ( $postData );
                  } */
                $mobile = $this->input->post('mobile');
                $randcode = rand(10000, 99999);
                $postData['otp'] = $randcode;

                /*
                 * Send otp
                 */
                $sender_id = 'TIPLUR';
                $user_id = urlencode('rakshat.chopra@gmail.com');
                $pwd = urlencode('RC@123tm');
                $message = urlencode("Dear Customer, Your OTP is " . $randcode . " Have a pleasant day!");

                $URL = "http://49.50.77.216/API/SMSHttp.aspx?UserId=" . $user_id . "&pwd=" . $pwd . "&Message=" . $message . "&Contacts=" . $mobile . "&SenderId=" . $sender_id . "";
                $ch = curl_init($URL);
                curl_setopt($ch, CURLOPT_URL, $URL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);
                curl_close($ch);

                $response->setStatus(1);
                $response->setMsg("Valid Mobile Number");
                $response->setObjArray($postData);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function products_listing() {
        $response = new response ();
        try {
            $apiDao = new Api_dao();
            $result = $apiDao->get_product_listing();
            if ($result) {
                $response->setStatus(1);
                $response->setMsg("success");
                $response->setObjArray($result);
            } else {

                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function app_info() {
        $response = new response ();
        try {
            $res = $this->db->select('*')->get('app_info')->row();
            if ($res) {
                $data['version'] = $res->version;
                $data['contact_no'] = $res->contact_no;
                $data['contact_email'] = $res->contact_email;
                $data['privacy'] = '';
                $data['faq'] = '';
                $data['about_us'] = '';

                $response->setStatus(1);
                $response->setMsg('success');
                $response->setObjArray($data);
            } else {
                $response->setStatus(0);
                $response->setMsg("Not Found");
                $response->setObjArray('');
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 24/10/2017
     * @method: update_cart
     * @desc: add/delete cart products
     * @params: action, product_id, retailer_id, quantity, price
     */

    public function update_cart() {
        $response = new response ();

        try {
            $cart = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->update_cart($cart);

            if ($result['status']) {
                $response->setStatus(1);
                $response->setMsg("Cart updated successfully.");
                $response->setObjArray(array("count" => $result['count']));
            } else {

                $response->setStatus(0);
                $response->setMsg($result['msg']);
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 24/10/2017
     * @method: get_cart
     * @desc: get cart detail
     * @params: user_id
     */

    public function get_cart() {
        $response = new response ();

        try {
            $cart = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->get_cart($cart);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Cart fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Cart is empty.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 24/10/2017
     * @method: add_address
     * @desc: add new address of user
     * @params: user_id, address, state, city, pincode
     */

    public function add_address() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $address = array('user_id' => $post['user_id'], 'state_id' => $post['state_id'], 'name' => $post['name'], 'mobile' => $post['mobile'], 'address' => $post['address'], 'city' => $post['city'], 'state' => $post['state'], 'pincode' => $post['pincode'], 'is_default' => $post['is_default']);
            $apiDao = new Api_dao();

            $is_available = $apiDao->is_available_in_state($post['state_id']);
            if ($is_available) {
                $result = $apiDao->add_address($address);

                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Address added successfully.");
                    $address['id'] = $result;
                    $address['state_id'] = $post['state_id'];
                    $address['is_default'] = $post['is_default'];
                    $response->setObjArray($address);
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Error in adding address.");
                    $response->setObjArray(NULL);
                }
            } else {
                $response->setStatus(0);
                $response->setMsg("Thanks for being on tiplur, as of now our services are not available here.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: address
     * @desc: get list of user address
     * @params: user_id
     */

    public function get_address() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $user_id = $post['user_id'];
            $apiDao = new Api_dao();
            $result = $apiDao->get_address($user_id);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Address fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No address found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: terms
     * @desc: get terms & conditions by state id
     * @params: state_id
     */

    public function get_terms() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $state_id = $post['state_id'];
            $is_retailer = $post['is_retailer'];
            $apiDao = new Api_dao();
            $result = $apiDao->get_terms($state_id, $is_retailer);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Terms & Conditions fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No content found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: empty_cart
     * @desc: empty cart data
     * @params: user_id, retailer_id
     */

    public function empty_cart() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->empty_cart($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Cart empty.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Error while deleting.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: request_order
     * @desc: order request by user
     * @params: user_id, retailer_id, address_id, array of product object (product_id, quantity, price)
     */

    public function request_order(){
    	$response = new response ();
    	
    	try
    	{
    		$post = $this->input->post();
    		$apiDao = new Api_dao();
    		$is_valid_valid = $apiDao->get_user_data($post['user_id']);
    		if($is_valid_valid->status==DOCUMENT_REJECTED){
    			$response->setStatus ( 2 );
    			$response->setMsg ( "Sorry! Your document is not approved. Please upload a valid document." );
    			$response->setObjArray ( NULL);
    		}else{
    			$result = $apiDao->complete_order($post);
    			
    			if($result['status']=='1')
    			{
    				
    				//is retailer logged out?
    				//$is_retailer_logged_out = $this->is_retailer_logged_out($post['retailer_id'], $result['msg']);
    				$response->setStatus ( 1 );
    				$response->setMsg ( "Order placed successfully." );
    				$response->setObjArray ( array("order_id"=>$result['msg']) );
    				
    			}else if($result=='-1'){
    				$response->setStatus ( 0 );
    				$response->setMsg ( $result['msg'] );
    				$response->setObjArray ( NULL);
    			}else{
    				$response->setStatus ( 0 );
    				$response->setMsg ( $result['msg']);
    				$response->setObjArray ( NULL);
    			}
    		}
    	} catch ( Exception $e ) {
    		$response->setStatus ( - 1 );
    		$response->setMsg ( $e->getMessage () );
    		$response->setError ( $e->getMessage () );
    		log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
    	}
    	return $response;
    	
    }

    /*
     * @author: Anjani Gupta
     * @date: 25/10/2017
     * @method: request_order
     * @desc: order request by user
     * @params: order_id(optional), retailer_id, user_id
     */

    public function order_history() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->order_history($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Order history fetched successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Error while fetching order history.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 26/10/2017
     * @method: update_rating
     * @desc: mark rating
     * @params: order_id, rating
     */

    public function update_rating() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->update_rating($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray(NULL);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 27/10/2017
     * @method: get_ratings
     * @desc: fetch all product ratings
     * @params: order_id(optional), user_id
     */

    public function get_ratings() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->get_ratings($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 28/10/2017
     * @method: update_price
     * @desc: Update product price
     * @params: product_id, user_id, price
     */

    public function update_price() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->update_price($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Price updated.");
                $response->setObjArray(NULL);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 28/10/2017
     * @method: remove_product
     * @desc: Remove product
     * @params: product_id, user_id
     */

    public function remove_product() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->remove_product($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Product deleted.");
                $response->setObjArray(NULL);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 28/10/2017
     * @method: requested_orders
     * @desc: list of requested orders
     * @params: retailer_id
     */

    public function requested_orders() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->requested_orders($post);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray($result);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 31/10/2017
     * @method: respond_order_request
     * @desc: confirm/decline order request
     * @params: order_id
     */

    public function respond_order_request() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            if ($post['status'] == ORDER_ACCEPTED) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is accepted successfully.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_DECLINED) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is declined successfully.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_ONGOING) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is out for delivery.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_DELIVERED) {

                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Order is delivered successfully.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } elseif ($post['status'] == ORDER_FAILED) {
                $result = $apiDao->respond_order_request($post);
                if ($result) {
                    $response->setStatus(1);
                    $response->setMsg("Success.");
                    $response->setObjArray("Sorry! This order is failed to deliver.");
                } else {
                    $response->setStatus(0);
                    $response->setMsg("Failed.");
                    $response->setObjArray(NULL);
                }
            } else {
                $response->setStatus(0);
                $response->setMsg("Sorry! This request is can not be proceed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: get_retailer_orders
     * @desc: Get retailer orders
     * @params: retailer_id
     */

    public function get_retailer_orders() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->get_retailer_orders($post['retailer_id']);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No data found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: retailer_order_detail
     * @desc: Get retailer order detail
     * @params: retailer_id, order_id
     */

    public function retailer_order_detail() {
        $response = new response ();

        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->retailer_order_detail($post);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("No data found.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 02/11/2017
     * @method: update_address
     * @desc: Update user address
     * @params: address_id
     */

    public function update_address() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->update_address($post);

            if (sizeof($result) > 0) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @author: Anjani Gupta
     * @date: 06/11/2017
     * @method: delete_address
     * @desc: Delete user address
     * @params: id
     */

    public function delete_address() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->delete_address($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Address deleted.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /*
     * @method: update_terms
     * @date: 09-11-2017
     * @params: state_id, is_accepted, user_id
     */

    public function update_terms() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->update_terms($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Terms & conditions accepted successfully.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed to accept terms & conditions.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    private function is_terms_accepted($user_id, $state_id) {
        $query = $this->db->where(array('user_id' => $user_id, 'state_id' => $state_id))->get('is_terms_accepted')->result();
        return $query ? $query : NULL;
    }

    /*
     * @method: holidays
     * @date: 14-11-2017
     * @params: state_id
     */

    public function get_holidays() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->get_holidays($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function retailer_rating() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->retailer_rating($post);

            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray(NULL);
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function request_count() {
        $response = new response ();

        try {
            $post = $this->input->post();

            $apiDao = new Api_dao();
            $result = $apiDao->request_count($post);

            $response->setStatus(1);
            $response->setMsg("Success.");
            $response->setObjArray($result);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /* check rating is exist for the users last order  30-11-2017 */

    public function check_rating_existence() {
        $response = new response ();
        try {
            $user_id = $this->input->post('user_id');
            $apiDao = new Api_dao();
            $result = $apiDao->checkorder_rating($user_id);
            if ($result) {
                $response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray($result);
            } else {
                $response->setStatus(0);
                $response->setMsg("Success.");
                $response->setObjArray("true");
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function is_email_exist() {
        $response = new Response ();
        $data = array();
        try {
            $apiDao = new Api_dao();

            $post = $this->input->post();
            $data['email'] = $post['email'];
            $response = $apiDao->is_email_exist($data);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    public function auto_reject_orders() {
        $apiDao = new Api_dao();
        $response = $apiDao->auto_reject_orders();
        return $response;
    }

    /**
     * @method: get_user_data
     * @param $user_id
     * @return user data
     */
    public function get_user_data() {
        $response = new Response ();
        try {
            $post = $this->input->post();
            $user_id = $post['user_id'];
            $apiDao = new Api_dao();
            $response = $apiDao->get_user_data($user_id);
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    /**
     * @method: logout
     * @param  $user_id, $device_id
     * @return user data
     */
    public function logout() {
        $response = new Response ();
        try {
            $post = $this->input->post();
            $apiDao = new Api_dao();
            $result = $apiDao->logout($post);
            if ($result) {
            	$user = $this->db->select('role_id, mobile')->where(array('id'=>$post['user_id']))->get('users')->row();
            	if($user->role_id==ROLE_RETAILER){
            		$this->logout_msg($user->mobile);
            	}
            	$response->setStatus(1);
                $response->setMsg("Success.");
                $response->setObjArray("");
            } else {
                $response->setStatus(0);
                $response->setMsg("Failed.");
                $response->setObjArray("");
            }
        } catch (Exception $e) {
            $response->setStatus(- 1);
            $response->setMsg($e->getMessage());
            $response->setError($e->getMessage());
            log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
        }
        return $response;
    }

    
    private function logout_msg($mobile){

    		//$mobile = $this->db->select('mobile')->where(array('id'=>$id))->get('users')->row()->mobile;

    		/*
    		 * Send otp
    		 */
    		$sender_id = 'TIPLUR';
    		$user_id = urlencode('rakshat.chopra@gmail.com');
    		$pwd     = urlencode('RC@123tm');
    		$message = urlencode("We observe that you are logged off from your Merchant Application. Kindly login so that you may receive orders.");
    		
    		$URL ="http://49.50.77.216/API/SMSHttp.aspx?UserId=".$user_id."&pwd=".$pwd."&Message=".$message."&Contacts=".$mobile."&SenderId=".$sender_id."";
    		$ch = curl_init($URL);
    		curl_setopt($ch, CURLOPT_URL, $URL);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
    		$result = curl_exec($ch);
    		
    		curl_close($ch);
    		
    	
    }
    
    public function test(){
    	$mobile = "9599766074";
    	$sender_id = 'TIPLUR';
    	$user_id = urlencode('rakshat.chopra@gmail.com');
    	$pwd     = urlencode('RC@123tm');
    	//$message  = urlencode("Dear Customer, Your OTP is  Have a pleasant day!");
    	$message = urlencode("We observe that you are logged off from your Merchant Application. Kindly login so that you may receive orders.");
    	
    	$URL ="http://49.50.77.216/API/SMSHttp.aspx?UserId=".$user_id."&pwd=".$pwd."&Message=".$message."&Contacts=".$mobile."&SenderId=".$sender_id."";
    	$ch = curl_init($URL);
    	curl_setopt($ch, CURLOPT_URL, $URL);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
    	$result = curl_exec($ch);
    	
    	curl_close($ch);
    }

}

?>