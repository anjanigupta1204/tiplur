<?php

class Cron extends MX_Controller{ 

    function __construct() {
    	error_reporting ( 0 );
    	$this->load->model ( 'api/Api_service' );
    	$this->load->model ( 'api/Api_dao' );
    	include_once './application/objects/Response.php';
    	$this->load->helper ( 'string' );
    	$this->load->library ( array (
    			'form_validation'
    	) );
    } 
	

    
    public function auto_reject_orders(){
    	
    	$apiService = new Api_service();
    	
    	$response = $apiService->auto_reject_orders();
    	$data =  array (
    			'status' => 1,
    			'message' => "Auto rejected orders",
    			'jsonData' => $response
    	);
    	echo json_encode($data);
    }
    
}
