<?php
$type = $this->input->get('type') ? decrypt($this->input->get('type')) : '';
if ($type == "1"):
    $name = "Add Sub Category";
    $url = "admin/category/create";
else:
    $name = "Add Category";
    $url = "admin/category/create?type=sub";
endif;
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/category' => 'Manage Category', $url => $name)); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1><?php echo $name ? $name : ''; ?></h1>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">
                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont ">
                                <div class="form-group formWidht">
                                    <label>Category Name <span style="color: red;">*</span></label>
                                    <input type="text" placeholder=""  name="title" value="<?php echo set_value('title', ''); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                </div>
                                <span class='error vlError'><?php echo form_error('title'); ?></span>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont category-textarea">
                                <div class="form-group formWidht">
                                    <label>Category Description <span style="color: red;">*</span></label>
                                    <textarea class="form-control"  name="description" style="width: 100%; height: 150px;"><?php echo set_value('description', ''); ?></textarea>
                                    <span class='error vlError'><?php echo form_error('description'); ?></span>
                                </div>
                            </div>

                            <?php if (isset($type) && $type == '1'): ?>
                                <div class="col-lg-4 col-md-4 col-sm-4	 AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Parent Category </label>
                                        <?php echo $parent_id = (!empty($_POST['parent_id'])) ? $_POST['parent_id'] : ''; ?>
                                        <select name="parent_id" required class="form-control m-b addContDrop product_category select-type1">
                                            <option value="" >Select</option>
                                            <?php product_cat($parent_id); ?>											
                                        </select>
                                        <span class='error vlError'><?php echo form_error('parent_id'); ?></span>
                                    </div>
                                </div>


                            <?php else : ?>
                                <input type="hidden" name="parent_id" id="parent_id" value="0" />
                            <?php endif; ?>

                            <!-- <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont" >

                                <div class="form-group formWidht">
                                    <label>Parent Category</label>                                   
                                    <select name="parent_id"  class="form-control m-b addContDrop">
                                        <option value="">None</option>
                                        <?php
                                        //$selected = (isset($_POST['parent_id']) && $_POST['parent_id']) ? $_POST['parent_id'] : '';
                                        //$cat_list = product_cat($selected);
                                        ?>                                        
                                    </select>
                                    <span class='error vlError'><?php echo form_error('parent_id'); ?></span>
                                </div>
                            </div> -->

                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont subcat_type" >

                                <div class="form-group formWidht">
                                    <label>Type *</label>
                                    <?php
                                    $sub_type = sub_category_type();
                                    $selected = (isset($_POST['type']) && $_POST['type']) ? $_POST['type'] : '';
                                    echo form_dropdown(array('name' => 'type', 'required' => 'required1', 'class' => 'form-control required  m-b addContDrop'), $sub_type, $selected);
                                    ?>
                                    <span class='error vlError'><?php echo form_error('type'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont subcat_type" >

                                <div class="form-group formWidht">
                                    <label>Category Type *</label>
                                    <?php
                                    $is_liquor = is_liquor();
                                    $selected = (isset($_POST['is_liquor']) && $_POST['is_liquor']) ? $_POST['is_liquor'] : '';
                                    echo form_dropdown(array('name' => 'is_liquor', 'required' => 'required1', 'class' => 'form-control required  m-b addContDrop'), $is_liquor, $selected);
                                    ?>
                                    <span class='error vlError'><?php echo form_error('is_liquor'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4" >
                            
                                <div class="form-group formWidht">
                                    <label>Category Image *</label>
                                    <input name="image" id="image" required="required" class="file dis-inline formWidht chooseBtnStyle" accept="xls" type="file">
                                    <span class='error vlError'><?php echo form_error('is_liquor'); ?></span>
                                </div>
                            
                        </div>
                        <input type="hidden" placeholder="" name="status" value="1" id="exampleInputEmail2" class="form-control formWidht">
                        <div class="ibox-content contentBorder">
                            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                <input type="submit" class="btn btn-primary block full-width m-b updateProductBtn" name="save" value="<?php echo $name;?>"/>
                            </div>
                        </div>

                    </div></div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
