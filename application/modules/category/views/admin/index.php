<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/category' => 'Manage category')); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php print_flash_message(); ?>
                <div class="ibox float-e-margins">

                    <div class="ibox-title" style="margin-bottom: 20px;">
                        <?php echo form_open($this->uri->uri_string(), array('method' => 'get')); ?>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Category Name</label>
                                    <input type="text" class="form-control" name="title" placeholder="Category Name" value="<?php echo isset($_GET['title']) ? $_GET['title'] : '' ?>">
                                </div>
                            </div>                            

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Parent Category</label>
                                    <select name="parent_id" class="form-control">
                                        <option value="">Please select</option>
                                        <?php
                                        $category = isset($_GET['parent_id']) ? $_GET['parent_id'] : null;
                                        product_cat($category);
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Category Type</label>
                                    <?php
                                    $sub_category_type = sub_category_type();
                                    $selected = (isset($_GET['type']) && $_GET['type']) ? $_GET['type'] : '';
                                    echo form_dropdown('type', $sub_category_type, $selected, 'class="form-control"');
                                    ?>
                                </div>                               
                            </div>

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Product Type</label>
                                    <?php
                                    $selected = (isset($_GET['is_liquor']) && $_GET['type']) ? $_GET['is_liquor'] : '';
                                    $type = product_type();
                                    echo form_dropdown('is_liquor', $type, $selected, 'class="form-control"');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group formWidht">
                                    <label>Status</label>
                                    <?php
                                    $state_id = isset($_GET['status']) ? $_GET['status'] : '';
                                    status_dropdown($state_id, 'form-control');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-2 filter-div">
                                <input type="submit" name="filter" id="" class="btn btn-success" value="Filter">
                                <?php if (isset($_GET['filter'])): ?>
                                    <a href="<?php echo base_url('admin/category'); ?>" class="btn btn-warning">Reset</a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="ibox-title">
                        <h2>View Category </h2>
                        <div class="ibox-tools" style="display: inline-block; float: right;">
                            <a class="btn btn-primary block full-width m-b catLogBtn" href="<?php echo base_url('admin/category/create'); ?>">ADD CATEGORY</a>
                        </div>
                    </div>
                    <div class="ibox-content borderNone">
                        <div class="table-responsive">
                            <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'id' => 'listing', 'method' => 'get')); ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center"><input type="checkbox"  class="i-checks check-all" name="check_all"></th>
                                        <th>Category Name</th>
                                        <th>Image</th>
                                        <th>Parent Category</th>
                                        <th>Type</th>
                                        <th>Is liquor</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>                                   
                                    <?php
                                    if ($result): $product_type = product_type();
                                        foreach ($result as $val):
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="i-checks child" name="cat_id[]" value="<?php echo $val->id; ?>">
                                                </td>
                                                <td><?php echo ucfirst($val->title); ?></td>
                                                <td>
                                                    <img class="img-rounded" width="100" height="50"src="<?php echo ($val->icon) ? base_url($val->icon) : base_url('/assets/images/profile_image/default-user.jpg'); ?>">
                                                </td>                                                
                                                <td><?php
                                                    $parent_link = '<a target="_blank" href="' . base_url('admin/category?parent_id=') . $val->parent_id . '&filter=Filter">' . $val->parent_name . '</a>';
                                                    echo ($val->parent_name) ? $parent_link : 'None';
                                                    ?>
												</td>
                                                <td><?php
                                                    if ($val->type == '1') {
                                                        echo "Domestic";
                                                    } else if ($val->type == '2') {
                                                        echo "Imported";
                                                    } else {
                                                        echo "";
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php
                                                    $liquer = is_liquor();
                                                    echo (!empty($val->is_liquor)) ? $liquer[$val->is_liquor] : $val->is_liquor;
                                                    ?>	
												</td>
                                                <td><?php echo print_status($val->status); ?>	</td>

                                                <td class="text-center"><a href="<?php echo base_url('admin/category/edit' . '/' . $val->id); ?>" ><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" class="delete" cat_id="<?php echo $val->id; ?>"  ><i class="fa fa-trash fa-lg" aria-hidden="true"  ></i></a>
												</td>
                                            </tr> 
                                                            <?php
                                                        endforeach;
                                                    else:
                                                        ?>
                                                        <tr> <td  class="text-center" colspan="12">Result not found.</td></tr>
                                                    <?php endif; ?>
                                                    </tbody>
                                                    </table>
                                                    </form>
                                                    <button name="multi_delete" form="listing" type="submit" class="viewDeleteButton delete-btn" id="delete-btn" value="multi_action">DELETE</button> 
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <div class="dataTables_info tableData" id="editable_info" role="status" aria-live="polite"></div>
                                                        </div>
                                                        <?php echo $this->pagination->create_links(); ?>
                                                        <!--div class="col-sm-7">
                                                                                    
                                                            <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                                                                                            
                                                                <ul class="pagination salesPagination">
                                                                    <li class="paginate_button previous disabled" id="editable_previous"><a href="#" aria-controls="editable" data-dt-idx="0" tabindex="0">Previous</a></li>
                                                                    <li class="paginate_button active"><a href="#" aria-controls="editable" data-dt-idx="1" tabindex="0">1</a></li>
                                                                    <li class="paginate_button "><a href="#" aria-controls="editable" data-dt-idx="2" tabindex="0">2</a></li>
                                                                    <li class="paginate_button "><a href="#" aria-controls="editable" data-dt-idx="3" tabindex="0">3</a></li>
                                                                    <li class="paginate_button "><a href="#" aria-controls="editable" data-dt-idx="4" tabindex="0">4</a></li>
                                                                    <li class="paginate_button "><a href="#" aria-controls="editable" data-dt-idx="5" tabindex="0">5</a></li>
                                                                    <li class="paginate_button "><a href="#" aria-controls="editable" data-dt-idx="6" tabindex="0">6</a></li>
                                                                    <li class="paginate_button next" id="editable_next"><a href="#" aria-controls="editable" data-dt-idx="7" tabindex="0">Next</a></li>
                                                                </ul>
                                                            </div>
                                                        </div-->
                                                    </div>
      </div>
</div>
</div>
