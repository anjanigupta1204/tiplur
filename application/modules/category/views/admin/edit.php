<?php
$parent = ($result->parent_id != '0' && $result->parent_id != '') ? $result->parent_id : '';
if ($parent):
    $name = "update sub category";
else:
    $name = "update category";
endif;
?>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <!--Breadcrumbs -->	
    <?php breadcrumbs(array('admin/category' => 'Manage Category', 'admin/category/edit/' . $this->uri->segment(4) => $name)); ?>
    <div class="row border-bottom">
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open_multipart($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title addCatH1">
                        <h1><?php echo $name ? $name : ''; ?></h1>
                        <div class="ibox-tools">
                        </div>
                    </div> 
                    <div class="ibox-content contentBorder ">
                        <div class="row contMargin">
                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Category Name <span>*</span></label>
                                    <input type="text" placeholder="" required name="title" value="<?php echo set_value('title', $result->title); ?>" id="exampleInputEmail2" class="form-control formWidht">
                                </div>
                                <span class='error vlError'><?php echo form_error('title'); ?></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont category-textarea">
                                <div class="form-group formWidht">
                                    <label>Category Description <span>*</span></label>
                                    <textarea class="form-control" required name="description" style="width: 100%; height: 150px;"><?php echo set_value('description', $result->description); ?></textarea>
                                    <span class='error vlError'><?php echo form_error('description'); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 AddProdctInputCont">
                                <div class="form-group formWidht">
                                    <label>Status   <span>*</span></label>
                                    <select name="status" required class="form-control m-b addContDrop product_category ">
                                        <?php
                                        $check = (isset($result->status) && $result->status == 1) ? $result->status : 2;
                                        $status = status();
                                        foreach ($status as $k => $val):
                                            ?>
                                            <option value="<?php echo $k; ?>" <?php echo ($k == @$check) ? 'selected' : ''; ?>  ><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                        <span class='error vlError'><?php echo form_error('status'); ?></span>
                                    </select>
                                </div>
                            </div> 

                            <?php
                            $parent_id = (isset($result->parent_id) || $result->parent_id != 0) ? $result->parent_id : '';
                            if ($parent_id):
                                ?>
                                <div class="col-lg-4 col-md-4 col-sm-4	AddProdctInputCont">
                                    <div class="form-group formWidht">
                                        <label>Parent Category </label>
                                        <select name="parent_id" required  class="form-control m-b addContDrop product_category select-type1">
                                            <option value="">None</option>
                                            <?php product_cat($parent_id); ?>
                                        </select>
                                        <span class='error vlError'><?php echo form_error('parent_id'); ?></span>
                                        </select>
                                    </div>
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="parent_id" value="0"/> 
                            <?php endif; ?>
                            <?php if (!empty($result->type)): ?> 
                                <div class="col-lg-4 col-md-4 col-sm-4	 AddProdctInputCont subcat_type">
                                    <div class="form-group formWidht">
                                        <label>Type</label>
                                        <?php
                                        $sub_type = sub_category_type();
                                        $selected = (isset($result->type) && $result->type != '') ? $result->type : '';
                                        echo form_dropdown('type', $sub_type, $selected, 'class="form-control  m-b addContDrop",required="required"');
                                        ?>
                                        <span class='error vlError'><?php echo form_error('type'); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4	 AddProdctInputCont subcat_type" >

                                    <div class="form-group formWidht">
                                        <label>Category Type *</label>
                                        <?php
                                        $is_liquor = is_liquor();
                                        $selected = (isset($_POST['is_liquor']) && $_POST['is_liquor']) ? $_POST['is_liquor'] : $result->is_liquor;
                                        echo form_dropdown(array('required' => 'required1', 'class' => 'form-control required  m-b addContDrop', 'disabled' => 'disabled'), $is_liquor, $selected);
                                        ?>  
                                        <input type="hidden" name="is_liquor" value="<?php echo $result->is_liquor; ?>">
                                        <span class='error vlError'><?php echo form_error('type'); ?></span>
                                    </div>
                                </div>
                            <?php else: ?>
                                <input type="hidden" name="is_liquor" value="<?php echo $result->is_liquor; ?>">
                                <input type="hidden" name="type" value="0">
                            <?php endif; ?>
                            <div class="col-lg-12 col-md-12 col-sm-12" >
                                <div class="col-lg-4 col-md-4 col-sm-4" >

                                    <div class="form-group formWidht">
                                        <label>Category Image *</label>
                                        <input name="image" id="image" <?php echo ($result->icon) ? '' : 'required="required" '; ?> class="file form-control formWidht chooseBtnStyle" accept="image/x-png,image/gif,image/jpeg"  type="file">
                                        <span class='error vlError'><?php echo form_error('is_liquor'); ?></span>
                                    </div>
                                    <?php if (!empty($result->icon)): ?>
                                        <input id='h_image' type='hidden' name='h_image'  value="<?php echo ($result->icon) ? $result->icon : ''; ?>" />
                                        <span class='help-inline'><img class="browseImage" src="<?php echo base_url() . $result->icon; ?>" height="100px" width="100"></span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 AddProdctInputCont">
                                <input style="padding: 0px 0px; " type="submit" class="btn btn-primary block full-width m-b updatePrdc" name="save" value="UPDATE CATEGORY"/>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
