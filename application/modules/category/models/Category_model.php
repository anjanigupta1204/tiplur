<?php

class Category_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Categorie listing 
    public function category_details($filter = null, $limit = null, $offset = null, $count = true) {

        if (isset($filter['title']) and $filter['title']) {
            $title = trim($filter['title']);
            $where = "( category.title LIKE '%$title%'  ESCAPE '!'  "
                    . "OR category.description LIKE '%$title%'  ESCAPE '!')";
            $this->db->where($where);
        }


        if (isset($filter['type']) and $filter['type']) {
            $this->db->where("category.type", $filter['type']);
        }
        if (isset($filter['is_liquor']) and $filter['is_liquor']) {
            $this->db->where("category.is_liquor", $filter['is_liquor']);
        }

        if (isset($filter['parent_id']) and $filter['parent_id']) {
            $parent_id = ($filter['parent_id'] != 'NAN') ? $filter['parent_id'] : '0';
            $this->db->where("category.parent_id", $parent_id);
        }


        if (isset($filter['status']) and $filter['status'] != '') {
			
            $status = ($filter['status'] == 2 ) ? 0 : $filter['status'];
            $this->db->where("category.status", $status);
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        //$this->db->where_in('category.status', array('0', '1'));
        //$this->db->where(array("category.is_deleted"=>'0'));
		$this->db->where(array("category.status!=" =>'3'));
        $res = $this->db->select("category.*, b.title as parent_name")
                        ->join("category as  b", "category.parent_id=b.id", "left")
                        ->order_by('category.id', 'desc')->get('category');

		//echo $this->db->last_query();exit;
        if ($count) {
            return $res->num_rows();
        } else {
            //echo $this->db->last_query();
            return $res->result();
        }
    }

    // form validation 
    public function get_form_validation_rules() {
        $form_validation = array(
            array(
                'field' => 'title',
                'label' => 'title',
                'rules' => 'required|min_length[3]|max_length[255]'
            ),
            array(
                'field' => 'type',
                'label' => 'type',
                'rules' => 'required'
            ),
            array(
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required'
            ),
            array(
                'field' => 'is_liquor',
                'label' => 'is liquor',
                'rules' => 'required'
            )
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

    // add categorie query
    public function add_categorie($data) {
        $res = $this->db->insert('category', $data);
        return ($res) ? $res : false;
    }

    // Categorie detail using id
    public function categorie_detail($id, $condition = false) {
        if ($condition == true) {
            $res = $this->db->select('id,title,IFNULL(description,"") AS description')->where(array('status' => '1', 'is_deleted' => '0', 'parent_id' => '0'))->get('category')->result();
        } else {
            $res = $this->db->where("id", $id)->get('category')->row();
        }
        return $res ? $res : false;
    }

    // Update categorie  
    public function update_categorie($data, $id = false) {
        $return = $this->db->where('id', $id)->update('category', $data);
        return $return;
    }

    // soft delete category   
    public function delete_cat($id) {

        $return = $this->db->where('id', $id)->update('category', array('status' => '3'));
        return $return;
    }

//    public function subcategory_listing($id) {
//        $result = $this->db->where('id', $id)->get('category_detail')->row();
//        if ($result) {
//            //Topics Array
//            $name = explode('<@>', $result->subcategory_name);
//            $id = explode('<@>', $result->subcategory_id);
//            $type = explode('<@>', $result->type);
//            $description = explode('<@>', $result->description);
//            if (isset($id)) {
//                $sub_array = array();
//                foreach ($id as $key => $val) {
//                    $sub_array['id'] = $val;
//                    $sub_array['name'] = $name[$key];
//                    $sub_array['type'] = $type[$key];
//                    $sub_array['description'] = $description[$key];
//                    $sub_array1[] = $sub_array;
//                }
//
//                $arr['category_id'] = $result->id;
//                $arr['category_name'] = $result->title;
//                $arr['subcategories'] = $sub_array1;
//            }
//            return $arr ? $arr : false;
//        } else {
//            return false;
//        }
//    }

}
