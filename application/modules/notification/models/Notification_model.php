<?php

class Notification_model extends CI_Model {

    protected $projectTable = "social_users";

    function __construct() {
        parent::__construct();
    }

    //  notification listing
//    function viewNotificationList($filter = null, $limit = null, $offset = null, $count = true) {
//        if (isset($filter['rname']) and $filter['rname']) {
//            $rname = trim($filter['rname']);
//            $this->db->where("notified_by", $rname);
//        }
//        if (isset($filter['sname']) and $filter['sname']) {
//            $sname = trim($filter['sname']);
//            $this->db->where("notified_to", $sname);
//        }
//        if (isset($filter['msg']) and $filter['msg']) {
//            $msg = trim($filter['msg']);
//            $this->db->like("message", $msg);
//        }
//        if (isset($filter['status']) and $filter['status']) {
//            $status = $filter['status'];
//            $this->db->where("seen", $status);
//        }
//
//        //date filter
//        if ($filter['from_date'] != '' && $filter['to_date'] != '') {
//            $this->db->where('DATE(created_on) >=', $filter['from_date']);
//            $this->db->where('DATE(created_on) <=', $filter['to_date']);
//        } else if ($filter['from_date'] != '') {
//            $this->db->where('DATE(created_on) >=', $filter['from_date']);
//        } else if ($filter['to_date'] != '') {
//            $this->db->or_where('DATE(created_on) <=', $filter['to_date']);
//        }
//
//
//        if ($limit) {
//            $this->db->limit($limit, $offset);
//        }
//        $result = $this->db->order_by('created_on', 'DESC')->get('notification_details');
//        return ($count) ? $result->num_rows() : $result->result();
//    }

    function users_listing() {
        $result = $this->db->where('deleted', '0')->get('users')->result();
        return $result;
    }

    // form validation 
    public function get_form_validation_rules() {
        $form_validation = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required'
            ),
            array(
                'field' => 'type',
                'label' => 'Type',
                'rules' => 'required'
            ),
            array(
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'required'
            )
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($form_validation);
        return ($this->form_validation->run() === false) ? false : true;
    }

}
