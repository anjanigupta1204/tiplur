 <?php  $count = unseen_notification_count($user_id); ?>
<a class="dropdown-toggle count-info" data-toggle="dropdown" onclick="update_notification()" href="javascript:void(0);">
                        <i class="fa fa-bell"></i>  <span class="label label-primary"><?php echo $count > 0 ? $count : ''; ?></span>
                    </a>
                       
                         
                        <ul class="dropdown-menu dropdown-alerts r">
                      <?php $records = unseen_notification_records($user_id); 
                      ?>
                                        <?php if ($records): ?>
                                            <?php foreach ($records as $record): ?>
                        <li>
                            <a href="javascript:void(0);">
                                <div>
                                <div class="user-img pull-left">
                                                            <strong><?php echo ucfirst(($record->notified_by_name)? $record->notified_by_name :'--'); ?></strong>
                                                            
                                                        </div>
                                                        </br>
                                                        <span><a href="#"><?php echo $record->message; ?></a></span>
                                    <span class="pull-right text-muted small"><?php echo time_ago($record->created_on); ?></span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <?php endforeach; ?>
                                        <?php else: ?>
                                            <li>There is no any new notification, for check old notifications please click on below button</li>
                                        <?php endif; ?>
                        
                                           <li class="divider"></li>
                                                     <li>
                            <div class="text-center link-block">
                                <a href="<?php echo base_url("admin/notification/view_notification"); ?>">
                                    <strong>See All Notification</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                       </ul>
                