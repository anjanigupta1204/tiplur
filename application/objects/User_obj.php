<?php
/**
*  This class define user object
*  Date 02/09/2015
@@@ author : pradeep keshari
 
**/
class User_obj{
		private $msg=null;
		private $userId=null;
		private $fname=null;
		private $lname=null;
		private $mobile=null;
		private $imei=null;
		private $password=null;
		private $email=null;
		private $street1=null;
		private $street2=null;
		private $country=null;
		private $countryCode=null;
		private $state=null;
		private $city=null;
		private $pin=null;
		private $latitude=null;
		private $longitude=null;
		private $mobile2=null;
		private $landline=null;
		private $affiliatedToDealer=null;
		private $activationCode=null;
		private $activationLinkVerified=null;
		private $branchCode=null;
		private $status=null;
		private $lastLoginDate=null;
		private $modificationDate=null;
		private $creationDate=null;
		private $termsAccepted=null;
		private $dealerCode = null;
		private $dealerName = null;

/*  Create construction of class */
		
	public function User_obj(){
	 	 $msg=null;
	 	 $userId=null;
		 $fname=null;
		 $lname=null;
		 $mobile=null;
		 $imei=null;
		 $password=null;
		 $email=null;
		 $street1=null;
		 $street2=null;
		 $country=null;
		 $countryCode=null;
		 $state=null;
		 $city=null;
		 $pin=null;
		 $latitude=null;
		 $longitude=null;
		 $mobile2=null;
		 $landline=null;
		 $affiliatedToDealer=null;
		 $activationCode=null;
		 $activationLinkVerified=null;
		 $status=null;
		 $lastLoginDate=null;
		 $modificationDate=null;
		 $creationDate=null;
		 $branchCode=null;
		 $termsAccepted=null;
		 $dealerCode=null;
		 $dealerName=null;
	}
	
	public function setId($id){
		$this->userId=$id;
	}
	public function getId(){
		return $this->userId;
	}
 
	public function setStatus($status){
		$this->status=$status;
	}
	public function getStatus(){
		return $this->status;
	}
	
	public function setfName($fname){
		$this->fname=$fname;
	}
	public function getfName(){
		return $this->fname;
	}
	
	public function setlName($lname){
		$this->lname=$lname;
	}
	public function getlName(){
		return $this->lname;
	}
	
	public function setPassword($password){
		$this->password = $password;
	}
	public function getPassword(){
		return $this->password;
	}
	
	public function setMsg($msg){
		$this->msg = $msg;
	}
	public function getMsg(){
		return $this->msg;
	}
	
	public function setEmail($email){
		$this->email=$email;
	}
	public function getEmail(){
		return $this->email;
	}
	
	public function setMobile($mobile){
		$this->mobile=$mobile;
	}
	public function getMobile(){
		return $this->mobile;
	}
	
	public function setImei($imei){
		$this->imei=$imei;
	}
	public function getImei(){
		return $this->imei;
	}
	
	public function setStreet1($street1){
		$this->street1=$street1;
	}
	public function getStreet1(){
		return $this->street1;
	}
	
	public function setStreet2($street2){
		$this->street2=$street2;
	}
	public function getStreet2(){
		return $this->street2;
	}
	
	public function setState($state){
		$this->state=$state;
	}
	public function getState(){
		return $this->state;
	}
	
	public function setCity($city){
		$this->city=$city;
	}
	public function getCity(){
		return $this->city;
	}
	
	public function setCountry($country){
		$this->country=$country;
	}
	public function getCountry(){
		return $this->country;
	}
	
	public function setCountryCode($countryCode){
		$this->countryCode=$countryCode;
	}
	public function getCountryCode(){
		return $this->countryCode;
	}
	
	public function setLatitude($latitude){
		$this->latitude=$latitude;
	}
	public function getLatitude(){
		return $this->latitude;
	}
	
	public function setLongitude($longitude){
		$this->longitude=$longitude;
	}
	public function getLongitude(){
		return $this->longitude;
	}
	
	public function setMobile2($mobile2){
		$this->mobile2=$mobile2;
	}
	public function getMobile2(){
		return $this->mobile2;
	}
	
	public function setLandline($landline){
		$this->landline=$landline;
	}
	public function getLandline(){
		return $this->landline;
	}
		
	public function setAffiliatedToDealer($affiliatedToDealer){
		$this->affiliatedToDealer=$affiliatedToDealer;
	}
	public function getAffiliatedToDealer(){
		return $this->affiliatedToDealer;
	}
	
	public function setActivationCode($activationCode){
		$this->activationCode=$activationCode;
	}
	public function getActivationCode(){
		return $this->activationCode;
	}
	
	public function setActivationLinkVerified($activationLinkVerified){
		$this->activationLinkVerified=$activationLinkVerified;
	}
	public function getActivationLinkVerified(){
		return $this->activationLinkVerified;
	}
	
	public function setLastLoginDate($lastLoginDate){
		$this->lastLoginDate=$lastLoginDate;
	}
	public function getLastLoginDate(){
		return $this->lastLoginDate;
	}
	
	public function setModificationDate($modificationDate){
		$this->modificationDate=$modificationDate;
	}
	public function getModificationDate(){
		return $this->modificationDate;
	}
	
	public function setCreationDate($creationDate){
		$this->creationDate=$creationDate;
	}
	public function getCreationDate(){
		return $this->creationDate;
	}
	
	public function setPin($pin){
		$this->pin=$pin;
	}
	public function getPin(){
		return $this->pin;
	}
	
	public function setTermsAccepted($termsAccepted){
		$this->termsAccepted=$termsAccepted;
	}
	public function getTermsAccepted(){
		return $this->termsAccepted;
	}
	
	public function setBranchCode($branchCode){
		$this->branchCode=$branchCode;
	}
	public function getBranchCode(){
		return $this->branchCode;
	}
	
	public function setDealerCode($dealerCode){
		$this->dealerCode=$dealerCode;
	}
	public function getDealerCode(){
		return $this->dealerCode;
	}
	
	public function setDealerName($dealerName){
		$this->dealerName=$dealerName;
	}
	public function getDealerName(){
		return $this->dealerName;
	}
	public function objectToArray( $object )
    {
        if( !is_object( $object ) && !is_array( $object ) )
        {
            return $object;
        }
        if( is_object( $object ) )
        {
			//$array = array();
        	$object = get_object_vars( $object );
	        
        }
        return array_map( array($this , 'objectToArray'), $object );
    }
	// End Of Calss
		
}
?>